<header class="news-header">
      <div class="news-header__container">

        <div style="display: flex; align-items: center; gap: 8px">
          <div class="menu-burger-inner-news">
            <div class="header__menu-icon menu-burger-news">
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>

          <div class="news-header__logo">
              <a href="/">
                <img src="/images/logo-text-shrink.svg" alt="logo" />
              </a>
            <p class="news-header__descr">New Culture Of Being Informed</p>
          </div>
        </div>


        <div class="right-news-header">
          <ul class="news-header__menu">
            <li class="news-header__menu-item header-select">
              <form class="select-mode select-mode__mode">
                <div class="__select selectMode selectModeMode" data-state="">
                  <div style="display: flex;align-items: center;" class="selectMode-open">
                    <div class="__select__title selectMode__title" data-default="{{ $mode }}">{{ $mode }} </div>
                    <img class="select-arrow" src="/images/icons/arrowDown.svg">
                  </div>
                  <div class="__select__content">
                      <input id="singleSelect1" data-mode="Highlights" class="__select__input" type="radio" name="singleSelect" @if($mode == 'Highlights') checked  @endif />
                      <label for="singleSelect1" class="__select__label">Highlights</label>
                      <input id="singleSelect0" data-mode="Statement" class="__select__input" type="radio" name="singleSelect" @if($mode == 'Statement') checked  @endif />
                      <label for="singleSelect0" class="__select__label">Statement</label>
                      <input id="singleSelect3" data-mode="Child mode" class="__select__input" type="radio" name="singleSelect" @if($mode == 'Child mode') checked  @endif  />
                      <label for="singleSelect3" class="__select__label">Child mode</label>
                      <input id="singleSelect4" data-mode="Summary" class="__select__input" type="radio" name="singleSelect" @if($mode == 'Summary') checked  @endif />
                      <label for="singleSelect4" class="__select__label">Summary</label>
                  </div>
                </div>
              </form>
              <script>
                  document.addEventListener('DOMContentLoaded', function() {
                      var selectInputs = document.querySelectorAll('.select-mode__mode .__select__input');

                      selectInputs.forEach(function(selectInput) {
                          selectInput.addEventListener('click', function() {
                              var mode = this.getAttribute('data-mode');
                              if (mode) {
                                  window.location.href = '/mode?mode='  +  mode + '&url=' + '{{ $url }}';
                              }
                          });
                      });
                  });
                </script>
            </li>
            <li class="news-header__menu-item header-select">
              <form class="select-mode select-mode__date">
                <div class="__select selectDate selectModeDate" data-state="">
                  <div style="display: flex;align-items: center;" class="selectDate-open">
                    <div class="__select__title selectDate__title" data-default="{{ $date }}">{{ $date }}</div>
                    <img class="select-arrow" src="/images/icons/arrowDown.svg">
                  </div>

                  <div class="__select__content selectDate_content">
                    <input id="selectDate1" data-mode="Today" class="__select__input" type="radio" name="selectDate" @if($mode == 'Today') checked  @endif/>
                    <label for="selectDate1" class="__select__label selectDate_labels">Today</label>
                    <input id="selectDate0" data-mode="Week" class="__select__input" type="radio" name="selectDate" @if($mode == 'Week') checked  @endif/>
                    <label for="selectDate0" class="__select__label selectDate_labels">Week</label>
                    <input id="selectDate3" data-mode="Month" class="__select__input" type="radio" name="selectDate" @if($mode == 'Month') checked  @endif />
                    <label for="selectDate3" class="__select__label selectDate_labels">Month</label>
                    <input id="selectDate4" data-mode="All Time" class="__select__input" type="radio" name="selectDate" @if($mode == 'All Time') checked  @endif  />
                    <label for="selectDate4" class="__select__label selectDate_labels">All time</label>
                  </div>
                </div>
              </form>
              <script>
                  document.addEventListener('DOMContentLoaded', function() {
                      var selectInputs = document.querySelectorAll('.select-mode__date .__select__input');

                      selectInputs.forEach(function(selectInput) {
                          selectInput.addEventListener('click', function() {
                              var mode = this.getAttribute('data-mode');
                              if (mode) {
                                  window.location.href = '/date?mode='  +  mode + '&url=' + '{{ $url }}';
                              }
                          });
                      });
                  });
                </script>
            </li>
            <li class="news-header__menu-item link--open-modal">Topics</li>
            <li class="news-header__menu-item link--open-modal-subscriptions">Subscriptions</li>
          </ul>
          <form action="/search" method="GET">
          <div class="search-box">
              <input type="text" name="search" value="{{ $search }}" placeholder="Search..." />
              <input type="hidden" name='url' value="{{ $url }}" style="visibility: hidden;">
              <input type="submit"  style="visibility: hidden;"/>
            </form>
            
            
              @if($search)
              <div class="search-btn">
                <a href="/search?url={{ $url }}"><img src="/images/icons/close.png" alt="close search"></a>
              </div>
              @else
              <button class="search-btn">
                <img src="/images/icons/search.png" alt="search"></a>
              </button>
              @endif
            

            <div class="search-cancel-btn">
              <img src="/images/icons/close.png" alt="close search">
            </div>
          </div>

          <div class="avatar-box">
            <style>
              .circle {
                max-height: 100%;
                max-width: 100%;
                border-radius: 50%;
                font-family: roboto, arial;
                color: white;
                line-height: 22px;
                text-align: center;
                background: orange;
              }
            </style>

            @if(auth()->user()?->image)
              <div class="avatar">
                <img src="{{ auth()->user()?->image }}" alt="avatar" />
              </div>
            @else
              <div class="avatar">
                <div class="circle">		
                  <script>
                    var initials = '{{ mb_substr(auth()->user()?->name, 0, 1) }}';
                    document.write(initials.toUpperCase()); 				// display in the html the value of the variable called 'initials' and capitalize them
                  </script>
                </div>
              </div>
            @endif
            <p class="avatar-name">{{ auth()->user()?->name }}</p>
            <script>
                document.addEventListener('DOMContentLoaded', function() {
                    var selectInputs = document.querySelectorAll('.avatar-btn-logOut');

                    selectInputs.forEach(function(selectInput) {
                        selectInput.addEventListener('click', function() {
                            var href = this.getAttribute('data-href');
                            if (href) {
                                window.location.href = href;
                            }
                        });
                    });
                });
            </script>
            <button class="avatar-btn-logOut" data-href="/logout"><img src="/images/news/logOut.svg" class='sign-out-img' /> Sign
              Out</button>
          </div>
        </div>
      </div>
    </header>