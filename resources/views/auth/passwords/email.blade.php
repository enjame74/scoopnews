<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>SCOOP - Intelligent News Feed - Recover password</title>
  <link rel="shortcut icon" href="/images/favicon/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon" href="/images/favicon/apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/apple-touch-icon-57x57.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/apple-touch-icon-72x72.png" />
  <link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/apple-touch-icon-76x76.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/apple-touch-icon-114x114.png" />
  <link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/apple-touch-icon-120x120.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/apple-touch-icon-144x144.png" />
  <link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/apple-touch-icon-152x152.png" />
  <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-touch-icon-180x180.png" />

  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link
    href="https://fonts.googleapis.com/css2?family=Manrope:wght@400;500;600;700&family=Unbounded:wght@700&family=Urbanist:wght@500;600;700;900&display=swap"
    rel="stylesheet">
  <link rel="stylesheet" href="/css/style.min.css">
  <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function() {
            var selectInputs = document.querySelectorAll('button');

            selectInputs.forEach(function(selectInput) {
                selectInput.addEventListener('click', function() {
                    var href = this.getAttribute('href');
                    if (href) {
                        window.location.href = href;
                    }
                });
            });
        });
    </script>
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NHN2837');</script>
<!-- End Google Tag Manager -->
</head>

<body>
  <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NHN2837"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
  <main class="login">
    <header class="header">
      <div class="header-logo">
        <a href="/"><img src="/images/landing/headerLogo.svg" alt="scoop logo"></a>
      </div>
      <ul class="header-menu">
        <li class="header-menu__item">

        </li>
        <li class="header-menu__item">
            <a href="/about">About</a>
        </li>
        <li class="header-menu__item">
            <a href="/#contacts">Contacts</a>
        </li>
      </ul>
      <div style="display: flex; align-items: center;">
        <div class="menu-burger-inner">
          <div class="header__menu-icon menu-burger">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
        <a href="/news/" class="header-btn">Enter</a>
      </div>

    </header>

    <div class="left-auth-main">
      <div class="left-auth-main-inner">
        <img src="/images/logo-text-shrink.svg" class="auth-logo" alt="logo" width="174">
        <form method="POST" action="{{ route('password.email') }}" class="auth-form">
            @csrf
          <h1 class="auth-title">Recover password</h1>

            @if (session('status'))
                <p class="auth-descr">We have sent password recovery instructions to your email</p>
            @else
                <p class="auth-descr">Enter your email . We will send you the instructions</p>
                <div class="input-box">
                    <label for="email">Email</label>
                    <input id="email" name="email" type="text" class="@error('email') input-error @enderror" placeholder="Your email"  required/>
                    
                    @error('email')
                        <div class="input-error-message">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn-transform auth-btn">
                    Send
                </button>
            @endif
        </form>
      </div>
    </div>

    <div class="right-auth-img">
      <img src="/images/auth/bg.jpg">
    </div>
  </main>

  <script src="/js/main.min.js"></script>
</body>

</html>