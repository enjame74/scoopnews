@php
  $mode = session()->get('mode', 'Highlights');

  function formatTextIntoArray($text) {
    $formattedLine = preg_replace('/\s?\d+\./', '##', $text);;
    $lines = explode('##', $formattedLine);
    $formattedLines = [];

    foreach ($lines as $key => $line) {
        if($key == 0) {
            continue;
        }

        $formattedLine = preg_replace('/^\d+\./', '##', $line);
        $formattedLine = trim($formattedLine); // добавлено: удаление пробелов в начале строки
        if (!empty($formattedLine)) {
            $formattedLines[] = $formattedLine;
        }
    }

    return $formattedLines;
}

  function createNumberedList($lines)
  {
      $numberedList = '';

      foreach ($lines as $index => $line) {
          $numberedList .= ($index + 1) . '. ' . $line . PHP_EOL;
      }

      return $numberedList;
  }

  function getChunckedItems($arr) {
    $result = array();
    $count = count($arr);

    if(count($arr) >= 20) {
      for ($i = 0; $i < $count; $i += 10) {
        $tens = array_slice($arr, $i, 10);
        $elements = array_slice($tens, 0, 1);
        $result = array_merge($result, $elements);
      }
    } else {
      for ($i = 0; $i < $count; $i += 10) {
        $tens = array_slice($arr, $i, 10);
        $elements = array_slice($tens, 0, 3);
        $result = array_merge($result, $elements);
      }
    }
    
    if ($count <= 10 || count($result) < 3) {
      $result = array_slice($arr, 0, 3);
    }

    return array_slice($result, 0, 3);
  }
@endphp



@foreach($news as $newsItem)
  <div class="news-item">
    <div class="news-item__logo">
    @if($newsItem->source == 'Medium')
      <img src="/images/news/logos/medium.svg" alt="logo-newspaper" />
      @elseif($newsItem->source == 'ESPN')
      <img src="/images/news/logos/espn.png" alt="logo-newspaper" />
      @elseif($newsItem->source == 'BBC News')
      <img src="/images/news/logos/bbc-news.png" alt="logo-newspaper" />
      @elseif($newsItem->source == 'Reddit /r/all')
      <img src="/images/news/logos/reddit.png" alt="logo-newspaper" />
      @elseif($newsItem->source == 'TechCrunch')
      <img src="/images/news/logos/tech-crunch.svg" alt="logo-newspaper" />
      @elseif($newsItem->source == 'Tech Crunch')
      <img src="/images/news/logos/tech-crunch.svg" alt="logo-newspaper" />
      @elseif($newsItem->source == 'Newsweek')
      <img src="/images/news/logos/newsweek.png" alt="logo-newspaper" />
      @elseif($newsItem->source == 'National Geographic')
      <img src="/images/news/logos/national-geographic.png" alt="logo-newspaper" />
      @elseif($newsItem->source == 'Independent')
      <img src="/images/news/logos/independent.png" alt="logo-newspaper" />
      @elseif($newsItem->source == 'National Review')
      <img src="/images/news/logos/national-review.png" alt="logo-newspaper" />
      @elseif($newsItem->source == 'CBC News')
      <img src="/images/news/logos/cbc.png" alt="logo-newspaper" />
      @elseif($newsItem->source == 'Engadget')
      <img src="/images/news/logos/engodget.png" alt="logo-newspaper" />
      @elseif($newsItem->source == 'Hacker News')
      <img src="/images/news/logos/hacker-news.png" alt="logo-newspaper" />
      @elseif($newsItem->source == 'Buzzfeed')
      <img src="/images/news/logos/buzz-feed.png" alt="logo-newspaper" />
      @elseif($newsItem->source == 'Fortune')
      <img src="/images/news/logos/fortune.png" alt="logo-newspaper" />
      @elseif($newsItem->source == 'CNN')
      <img src="/images/news/logos/cnn.png" alt="logo-newspaper" />
      @elseif($newsItem->source == 'Associated Press')
      <img src="/images/news/logos/ap.png" alt="logo-newspaper" />
      @elseif($newsItem->source == 'Reuters')
      <img src="/images/news/logos/reuters.png" alt="logo-newspaper" />
      @elseif($newsItem->source == 'The Huffington Post')
      <img src="/images/news/logos/huffpost.png" alt="logo-newspaper" />
      @elseif($newsItem->source == 'Bild')
      <img src="/images/news/logos/bild.png" alt="logo-newspaper" />
      @elseif($newsItem->source == 'TheHustle')
      <img src="/images/news/logos/hustle.svg" alt="logo-newspaper" />
      @elseif($newsItem->source == 'Insider')
      <img src="/images/news/logos/insider.svg" alt="logo-newspaper" />
      @elseif($newsItem->source == 'Bloomberg')
      <img src="/images/news/logos/bloomberg.svg" alt="logo-newspaper" />
      @elseif($newsItem->source == 'Business Insider')
      <img src="/images/news/logos/business-insider.svg" alt="logo-newspaper" />
      @elseif($newsItem->source == 'Mashable')
      <img src="/images/news/logos/mashable.svg" alt="logo-newspaper" />
      @elseif($newsItem->source == 'New York Magazine')
      <img src="/images/news/logos/newyork.svg" alt="logo-newspaper" />
      @elseif($newsItem->source == 'Politico')
      <img src="/images/news/logos/politico.svg" alt="logo-newspaper" />
      @elseif($newsItem->source == 'TechRadar')
      <img src="/images/news/logos/techradar.svg" alt="logo-newspaper" />
      @elseif($newsItem->source == 'Tech Radar')
      <img src="/images/news/logos/techradar.svg" alt="logo-newspaper" />
      @elseif($newsItem->source == 'The Verge')
      <img src="/images/news/logos/theverge.svg" alt="logo-newspaper" />
      @elseif($newsItem->source == 'Time')
      <img src="/images/news/logos/time.svg" alt="logo-newspaper" />
      @elseif($newsItem->source == 'Wired')
      <img src="/images/news/logos/wired.svg" alt="logo-newspaper" />
      @elseif($newsItem->source == 'WSJ')
      <img src="/images/news/logos/wsj.svg" alt="logo-newspaper" />
      @else
        <img src="/images/news/logos/default.svg" alt="logo-newspaper" />
      @endif
    </div>

    <div class="news-item__info">
      <div class="news-item__header">
        <div style="display: flex; gap: 8px; align-items: center; align-self: flex-start;">
          <div class="news-item__logo--mobile">
                @if($newsItem->source == 'Medium')
                <img src="/images/news/logos/medium.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'ESPN')
                <img src="/images/news/logos/espn.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'BBC News')
                <img src="/images/news/logos/bbc-news.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Reddit /r/all')
                <img src="/images/news/logos/reddit.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'TechCrunch')
                <img src="/images/news/logos/tech-crunch.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Tech Crunch')
                <img src="/images/news/logos/tech-crunch.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Newsweek')
                <img src="/images/news/logos/newsweek.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'National Geographic')
                <img src="/images/news/logos/national-geographic.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Independent')
                <img src="/images/news/logos/independent.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'National Review')
                <img src="/images/news/logos/national-review.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'CBC News')
                <img src="/images/news/logos/cbc.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Engadget')
                <img src="/images/news/logos/engodget.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Hacker News')
                <img src="/images/news/logos/hacker-news.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Buzzfeed')
                <img src="/images/news/logos/buzz-feed.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Fortune')
                <img src="/images/news/logos/fortune.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'CNN')
                <img src="/images/news/logos/cnn.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Associated Press')
                <img src="/images/news/logos/ap.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Reuters')
                <img src="/images/news/logos/reuters.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'The Huffington Post')
                <img src="/images/news/logos/huffpost.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Bild')
                <img src="/images/news/logos/bild.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'TheHustle')
                <img src="/images/news/logos/hustle.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Insider')
                <img src="/images/news/logos/insider.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Bloomberg')
                <img src="/images/news/logos/bloomberg.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Business Insider')
                <img src="/images/news/logos/business-insider.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Mashable')
                <img src="/images/news/logos/mashable.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'New York Magazine')
                <img src="/images/news/logos/newyork.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Politico')
                <img src="/images/news/logos/politico.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'TechRadar')
                <img src="/images/news/logos/techradar.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Tech Radar')
                <img src="/images/news/logos/techradar.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'The Verge')
                <img src="/images/news/logos/theverge.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Time')
                <img src="/images/news/logos/time.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Wired')
                <img src="/images/news/logos/wired.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'WSJ')
                <img src="/images/news/logos/wsj.svg" alt="logo-newspaper" />
                @else
                  <img src="/images/news/logos/default.svg" alt="logo-newspaper" />
                @endif
          </div>
          <h3 class="news-item__name">{{ $newsItem->source }}</h3>
        </div>

        <div class="news-item__data">
          <p>{{ $newsItem ->created_at->format('Y-m-d') }}</p>
          <div class="separator"></div>
          <p>{{ $newsItem ->created_at->format('H:i') }}</p>
        </div>
      </div>
      <ul class="news-item__tags">
        @php
            $newsTags = $newsItem->tags;
            $allTags = [];

            foreach($newsTags as $newsTag) {
            $name = $newsTag->name;

            if (strlen($name) > 3 && strlen($name) < 15) { $allTags[]=$name; } } $newsTags=array_slice(array_unique($allTags),
              0, 7); $otherCount=count($allTags) - 3; @endphp
            @foreach($newsTags as $tag) <li><a href="/news/tags/{{ $tag }}">#{{ $tag }}</a></li> @endforeach
      </ul>
      <div class="news-item__text">
        <p>
          {{ $newsItem->name }}
        </p>

          @if($mode == 'Highlights')
            @php
              $list = createNumberedList(getChunckedItems(formatTextIntoArray($newsItem->content_quote_en)));
            @endphp
          <p style="white-space: pre-line;">{{ $list }}</p>
          @elseif($mode == 'Child mode')
          <p>{{ $newsItem->content_redneck_en }}</p>
          @elseif($mode == 'Summary')
          <p>{{ $newsItem->content_ext_en }}</p>
          @else
          <p>{{ $newsItem->content_short_en }}</p>
          @endif
      </div>
      <div class="news-item__footer">
        <div class="news-item__footer-left">
          <div class="news-item__footer-item">
            <img src="/images/news/save.svg">
            <p>{{ rand(100, 2000) }}</p>
          </div>
          <div class="separator"></div>
          <div class="news-item__footer-item">
            <img src="/images/news/share.svg">
            <p>{{ rand(100, 2000) }}</p>
          </div>
        </div>
        <a target="_blank" href="{{ $newsItem->source_url }}" class="sourse-link">Source <img src="/images/news/sourse.svg"></a>
      </div>
    </div>
  </div>
  
  @endforeach