<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Scoop - news</title>
  <link rel="preconnect" href="https://fonts.googleapis.com" />
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
  <link
    href="https://fonts.googleapis.com/css2?family=Manrope:wght@400;500;600;700&family=Unbounded:wght@700&family=Urbanist:wght@500;600;700;900&display=swap"
    rel="stylesheet" />
  <link rel="stylesheet" href="/css/style.min.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.4/jquery.min.js" integrity="sha512-pumBsjNRGGqkPzKHndZMaAG+bir374sORyzM3uulLV14lN5LyykqNk8eEeUlUkB3U0M4FApyaHraT65ihJhDpQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-infinitescroll/4.0.1/infinite-scroll.pkgd.min.js" integrity="sha512-R50vU2SAXi65G4oojvP1NIAC+0WsUN2s+AbkXqoX48WN/ysG3sJLiHuB1yBGTggOu8dl/w2qsm8x4Xr8D/6Hug==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>

@php
  $mode = session()->get('mode', 'Default');

  function formatTextIntoArray($text) {
    $formattedLine = preg_replace('/\s?\d+\./', '##', $text);;
    $lines = explode('##', $formattedLine);
    $formattedLines = [];

    foreach ($lines as $key => $line) {
        if($key == 0) {
            continue;
        }

        $formattedLine = preg_replace('/^\d+\./', '##', $line);
        $formattedLine = trim($formattedLine); // добавлено: удаление пробелов в начале строки
        if (!empty($formattedLine)) {
            $formattedLines[] = $formattedLine;
        }
    }

    return $formattedLines;
}

  function createNumberedList($lines)
  {
      $numberedList = '';

      foreach ($lines as $index => $line) {
          $numberedList .= ($index + 1) . '. ' . $line . PHP_EOL;
      }

      return $numberedList;
  }

  function getChunckedItems($arr) {
    $result = array();
    $count = count($arr);

    if(count($arr) >= 20) {
      for ($i = 0; $i < $count; $i += 10) {
        $tens = array_slice($arr, $i, 10);
        $elements = array_slice($tens, 0, 1);
        $result = array_merge($result, $elements);
      }
    } else {
      for ($i = 0; $i < $count; $i += 10) {
        $tens = array_slice($arr, $i, 10);
        $elements = array_slice($tens, 0, 3);
        $result = array_merge($result, $elements);
      }
    }
    
    if ($count <= 10 || count($result) < 3) {
      $result = array_slice($arr, 0, 3);
    }

    return array_slice($result, 0, 3);
  }
@endphp

<body>
  <div class="news">
    <header class="news-header">
      <div class="news-header__container">
        <div class="news-header__logo">
          <a href="/"><img src="/images/logo-text.svg" alt="logo" /></a>
          <p class="news-header__descr">New Culture Of Being Informed</p>
        </div>

        <div class="right-news-header">
          <div style="display: flex; align-items: center;">
            <div class="menu-burger-inner-news">
              <div class="header__menu-icon menu-burger-news">
                <span></span>
                <span></span>
                <span></span>
              </div>
            </div>
          </div>
          <ul class="news-header__menu">
            <li class="news-header__menu-item link--open-modal"> Topics</li>
            <li class="news-header__menu-item" ><a href="/about">About</a></li>
          </ul>
          <div class="avatar-box">
            <style>
              .circle {
                max-height: 100%;
                max-width: 100%;
                border-radius: 50%;
                font-family: roboto, arial;
                color: white;
                line-height: 22px;
                text-align: center;
                background: orange;
              }
            </style>

            @if(auth()->user()?->image)
              <div class="avatar">
                <img src="{{ auth()->user()?->image }}" alt="avatar" />
              </div>
            @else
              <div class="avatar">
                <div class="circle">		
                  <script>
                    var initials = '{{ mb_substr(auth()->user()?->name, 0, 1) }}';
                    document.write(initials.toUpperCase()); 				// display in the html the value of the variable called 'initials' and capitalize them
                  </script>
                </div>
              </div>
            @endif
            <p class="avatar-name">{{ auth()->user()?->name }}</p>
            <script>
                document.addEventListener('DOMContentLoaded', function() {
                    var selectInputs = document.querySelectorAll('.avatar-btn-logOut');

                    selectInputs.forEach(function(selectInput) {
                        selectInput.addEventListener('click', function() {
                            var href = this.getAttribute('data-href');
                            if (href) {
                                window.location.href = href;
                            }
                        });
                    });
                });
            </script>
            <button class="avatar-btn-logOut" data-href="/logout"><img src="/images/news/logOut.svg" class='sign-out-img' /> Sign
              Out</button>
          </div>
        </div>
      </div>
    </header>
    <main class="news-main">
      <div class="news-container">
        <aside class="news-aside">
          <div class="news-style-img">
            @if($mode == 'Mr. President mode')
            <img src="/images/news/presidend.png" id="news-mode-img" />
            @elseif($mode == 'Child mode')
            <img src="/images/news/child.png" id="news-mode-img" />
            @elseif($mode == 'Preacher mode')
            <img src="/images/news/preacher.png" id="news-mode-img" />
            @else
            <img src="" id="news-mode-img" style="display: none;" />
            @endif
          </div>
          <div class="aside-bottom">
            <form class="select-mode">
              <div class="__select" data-state="">
                <div class="__select__title" data-default="{{ $mode }}">{{ $mode }}</div>
                <div class="__select__content">
                  <input id="singleSelect0" data-mode="Default" class="__select__input" type="radio" name="singleSelect" @if($mode == 'Default') checked  @endif />
                  <label for="singleSelect0" class="__select__label">Default</label>
                  <input id="singleSelect1" data-mode="Mr. President mode" class="__select__input" type="radio" name="singleSelect" @if($mode == 'Mr. President mode') checked  @endif />
                  <label for="singleSelect1" class="__select__label">Mr. President mode</label>

                  <input id="singleSelect3" data-mode="Child mode" class="__select__input" type="radio" name="singleSelect" @if($mode == 'Child mode') checked  @endif  />
                  <label for="singleSelect3" class="__select__label">Child mode</label>
                  <input id="singleSelect4" data-mode="Preacher mode" class="__select__input" type="radio" name="singleSelect" @if($mode == 'Preacher mode') checked  @endif />
                  <label for="singleSelect4" class="__select__label">Preacher mode</label>
                </div>
              </div>
              <script>
                document.addEventListener('DOMContentLoaded', function() {
                    var selectInputs = document.querySelectorAll('.__select__input');

                    selectInputs.forEach(function(selectInput) {
                        selectInput.addEventListener('click', function() {
                            var mode = this.getAttribute('data-mode');
                            if (mode) {
                                window.location.href = '/mode?mode='  +  mode;
                            }
                        });
                    });
                });
            </script>
            </form>
            <div class="news-aside-desktop">
              <ul class="aside__menu">
                <li><a target="_blank"  href="/policy">Privacy Policy</a></li>
                <li><a target="_blank" href="/terms">Terms of Use</a></li>
              </ul>
              <p class="right">© Scoop, 2023</p>
            </div>

          </div>
        </aside>
        <div class="news-news">

          @if($mode == 'Mr. President mode')
          <p class="news-title" id="news-title">
            Dear Sir, let me introduce you to the current world situation how I see it.
          </p>
          @elseif($mode == 'Child mode')
          <p class="news-title" id="news-title">
            Hi, grown ups! Let me try to tell you what I think about what I know :)
          </p>
          @elseif($mode == 'Preacher mode')
          <p class="news-title" id="news-title">
            Good day and welcome to this place of worship to honor the news from the world.
          </p>
          @else
          <p class="news-title" id="news-title">
            Hey, this is SCOOP, stay informed in a fast and entertaining way with us.
          </p>
          @endif
          
          <div class="news-list">
            @foreach($news as $newsItem)
            <div class="news-item">
              <div class="news-item__logo">
                @if($newsItem->source == 'Medium')
                <img src="/images/souce/medium.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Tech Crunch')
                <img src="/images/souce/tech-crunch.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'TheHustle')
                <img src="/images/souce/hustle.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Insider')
                <img src="/images/souce/insider.svg" alt="logo-newspaper" />
                @else
                  <img src="/images/souce/default.svg" alt="logo-newspaper" />
                @endif
              </div>

              <div class="news-item__info">
                <div class="news-item__header">
                  <div style="display: flex; gap: 8px; align-items: center; align-self: flex-start;">
                    <div class="news-item__logo--mobile">
                      @if($newsItem->source == 'Medium')
                      <img src="/images/souce/medium.svg" alt="logo-newspaper" />
                      @elseif($newsItem->source == 'Tech Crunch')
                      <img src="/images/souce/tech-crunch.svg" alt="logo-newspaper" />
                      @elseif($newsItem->source == 'TheHustle')
                      <img src="/images/souce/hustle.svg" alt="logo-newspaper" />
                      @elseif($newsItem->source == 'Insider')
                      <img src="/images/souce/insider.svg" alt="logo-newspaper" />
                      @else
                        <img src="/images/souce/default.svg" alt="logo-newspaper" />
                      @endif
                    </div>
                    <h3 class="news-item__name">{{ $newsItem->source }}</h3>
                  </div>

                  <div class="news-item__data">
                    <p>{{ $newsItem ->created_at->format('Y-m-d') }}</p>
                    <div class="separator"></div>
                    <p>{{ $newsItem ->created_at->format('H:i') }}</p>
                  </div>
                </div>
                <ul class="news-item__tags">
                </ul>
                <div class="news-item__text">
                  <p>
                    {{ $newsItem->name }}
                  </p>
      
                  <p>{{ $newsItem->content_en }}</p>
                </div>
                <div class="news-item__footer">
                  <div class="news-item__footer-left">
                    <div class="news-item__footer-item">
                      <img src="/images/news/save.svg">
                      <p>{{ rand(100, 2000) }}</p>
                    </div>
                    <div class="separator"></div>
                    <div class="news-item__footer-item">
                      <img src="/images/news/share.svg">
                      <p>{{ rand(100, 2000) }}</p>
                    </div>
                  </div>
                  <a target="_blank" href="{{ $newsItem->source_url }}" class="sourse-link">Source <img src="/images/news/sourse.svg"></a>
                </div>
              </div>
            </div>
            
            @endforeach


        <div class="news-mobile-footer">
          <ul class="aside__menu">
            <li><a target="_blank" href="/policy">Privacy Policy</a></li>
            <li><a target="_blank" href="/terms">Terms of Use</a> </li>
          </ul>
          <p class="right">© Scoop, 2023</p>
        </div>
      </div>
    </main>
  </div>

  <div class="modal">
    <div class="modal-outer">
      <button type="button" class="btn--close-modal">
        <svg width="24" height="24" viewBox="0 0 24 24">
          <rect width="24" height="24" rx="12" />
          <path
            d="M3 12C3 7.02944 7.02944 3 12 3V3C16.9706 3 21 7.02944 21 12V12C21 16.9706 16.9706 21 12 21V21C7.02944 21 3 16.9706 3 12V12Z" />
          <path fill-rule="evenodd" clip-rule="evenodd"
            d="M6.7045 15.7201C6.26517 16.1594 6.26517 16.8717 6.7045 17.311C7.14384 17.7504 7.85616 17.7504 8.2955 17.311L12.0533 13.5533L15.8111 17.3111C16.2504 17.7504 16.9628 17.7504 17.4021 17.3111C17.8414 16.8717 17.8414 16.1594 17.4021 15.7201L13.6443 11.9623L17.3111 8.29543C17.7504 7.85609 17.7504 7.14378 17.3111 6.70444C16.8718 6.2651 16.1595 6.2651 15.7201 6.70444L12.0533 10.3713L8.38649 6.70448C7.94715 6.26514 7.23483 6.26514 6.7955 6.70448C6.35616 7.14382 6.35616 7.85613 6.7955 8.29547L10.4623 11.9623L6.7045 15.7201Z"
            fill="#ffffff" />
        </svg>
      </button>
      <div class="modal-wrapper">
        <div class="modal-inner">
          <div class="modal-header">
            <h4 class="modal-title">Topics</h4>
            <script type="text/javascript">
                document.addEventListener('DOMContentLoaded', function() {
                    var selectInputs = document.querySelectorAll('.accordion-item button');
            
                    selectInputs.forEach(function(selectInput) {
                        selectInput.addEventListener('click', function() {
                            var href = this.getAttribute('href');
                            if (href) {
                                window.location.href = href;
                            }
                        });
                    });
                });

                document.addEventListener('DOMContentLoaded', function() {
                    var selectInputs = document.querySelectorAll('.accordion-item span');
            
                    selectInputs.forEach(function(selectInput) {
                        selectInput.addEventListener('click', function() {
                            var href = this.getAttribute('href');
                            if (href) {
                                window.location.href = href;
                            }
                        });
                    });
                });

                document.addEventListener('DOMContentLoaded', function() {
                    var selectInputs = document.querySelectorAll('.clear-all-btn');
            
                    selectInputs.forEach(function(selectInput) {
                        selectInput.addEventListener('click', function() {
                            var href = this.getAttribute('href');
                            if (href) {
                                window.location.href = href;
                            }
                        });
                    });
                });
            </script>
            <button href="/news/" class="clear-all-btn">Clear all</button>
          </div>
          <div class="accordion">
          @foreach($categories as $key=> $topic)
            <div class="accordion-item">
              <button  class="accordion-expand-btn" id="accordion-button-1" aria-expanded="false"><span
                  class="accordion-title">{{ ucfirst($key) }}</span><span class="icon" aria-hidden="true"></span></button>
              <div class="accordion-content">
                <div class="tab-filter__buttons">
                  <button href="/news/category/{{ $key }}" class="tab-filter__button select-all">Select All</button>
                </div>
              </div>
            </div>
          @endforeach

          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="/js/main.min.js"></script>
</body>

</html>