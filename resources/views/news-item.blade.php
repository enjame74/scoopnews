<!DOCTYPE html>
<html lang="en">
    

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title> {{ $newsItem->name }}</title>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
        href="https://fonts.googleapis.com/css2?family=Jost:wght@500&family=Manrope:wght@500;600;700;800&family=Urbanist:wght@500;600;700;800&display=swap"
        rel="stylesheet" />
    <link rel="stylesheet" href="/css/style.min.css" />
</head>

<body>
    @php
        $newsTags = $newsItem->tags;
        $allTags = [];

        foreach($newsTags as $newsTag) {
            $name = $newsTag->name;

            if (strlen($name) > 3 && strlen($name) < 15) {
            $allTags[] = $name;
            }
        }

        $tags = $allTags;
    @endphp
    <div class="g-wrapper">
        <header class="header">
            <div class="g-container">
                <div class="header__inner">
                    <div class="header__logo"> <a href="/">
                            <picture>
                                <source srcset="/images/logo.svg" type="image/webp"><img src="images/logo.svg"
                                    alt="logo Scoop" />
                            </picture>
                        </a> 
                    </div>
                    <nav class="header-action__navigation">
                        <ul class="header-action__menu">
                            <li> <a href="/news/"> News </a> </li>
                            <li> <a href="#"> Subscriptions </a> </li>
                            <li> <a href="#"> About </a> </li>
                        </ul>
                        <div class="header-action__search g-search-input" tabindex="0"> <input type="text"
                                placeholder="Search" aria-label="Search input" /> <button type="submit"
                                aria-label="Search button"> <i class="ui_search"></i> </button> </div> <a href="/login"
                            class="g-main-btn" aria-label="Sign in">Sign in</a>
                    </nav>
                </div>
            </div>
        </header>
        <main class="article-page">
            <div class="article-container">
                <div class="article-page__inner">
                    <article class="article">
                        <div class="article__header">
                            <div class="article__logo">
                                <a href="{{ $newsItem->source_url }}">
                                    {{ $newsItem->source }}
                                </a>
                            </div>
                            <div class="article__data-info">
                                <div class="article__data-info-item">
                                    <p>{{ $newsItem ->created_at->format('Y-m-d') }}</p>
                                </div>
                                <div class="article__data-info-item">
                                    <p>{{ $newsItem ->created_at->format('H:i') }}</p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <h1 class="article__title"> {{ $newsItem->name }} </h1>
                            <!-- <p class="article__descr"> As SVB careened towards catastrophe, some 50 founders, VCs,
                                economists and comms experts gathered in a WhatsApp group to draft a memo calling for
                                urgent preservation of its deposits for the sake of the broader economy. Then they sent
                                it to Washington. As SVB careened towards catastrophe, some 50 founders, VCs, economists
                                and comms experts gathered in a WhatsApp group to draft a memo economy calling for
                                urgent preservation of its deposits for the sake of the broader economy. </p> -->
                                
                            <div class="article__tags"> 
                                @foreach($tags as $tag)
                                     <button class="article__tag">{{ $tag }}</button> 
                                 @endforeach
                            </div>
                        </div>
                        <div class="article__content">
                            <div class="article__img-block">
                                <div class="article__img">
                                    <picture>
                                        <source srcset="{{ $newsItem->preview }}"><img
                                            src="{{ $newsItem->preview }}" />
                                    </picture>
                                </div>
                                <!-- <p class="article__img-descr">As SVB careened towards catastrophe</p> -->
                            </div>
                            <style type="text/css">
                                .spoiler-content {
                                    display: none;
                                    padding: 1rem;
                                    background-color: #f0f0f0;
                                    border-radius: 5px;
                                }

                                .spoiler[data-open="true"] .spoiler-content {
                                    display: block;
                                }
                            </style>
                            <script>
                                function toggleSpoiler(event) {
                                    const spoiler = event.target.closest('.spoiler');
                                    const content = spoiler.querySelector('.spoiler-content');
                                    const isOpen = spoiler.getAttribute('data-open') === 'true';

                                    if (isOpen) {
                                        spoiler.setAttribute('data-open', 'false');
                                        content.style.display = 'none';
                                    } else {
                                        spoiler.setAttribute('data-open', 'true');
                                        content.style.display = 'block';
                                    }
                                }
                            </script>
                            <div class="spoiler" data-open="false">
                                <button class="spoiler-toggle article__tag" onclick="toggleSpoiler(event)">Key points</button>
                                
                                <div class="spoiler-content">
                                    <div class="article__text-block">
                                        <!-- <h4 class='article__text-block-title'>50 Tech Leaders Circulated</h5> -->
                                            <p style="white-space: pre-line;">
                                                    @php
                                                        $lines = explode(PHP_EOL, $newsItem->content_quote_en);
                                                        $formattedLines = [];
                                                    
                                                        foreach ($lines as $key => $line) {
                                                            if($key == 0) {
                                                                continue;
                                                            }

                                                            $formattedLine = preg_replace('/^\d+\./', '', $line);
                                                            $formattedLine = trim($formattedLine); // добавлено: удаление пробелов в начале строки
                                                            if (!empty($formattedLine)) {
                                                                $formattedLines[] = $formattedLine;
                                                            }
                                                        }

                                                        $formattedLines = array_slice($formattedLines, 0, 10);

                                                    @endphp 

                                                    @foreach($formattedLines as $key=> $line)
                                                        {{ ++$key }} {{ $line }}
                                                    @endforeach
                                            </p>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="article__text-block"> -->
                                <!-- <h4 class='article__text-block-title'>50 Tech Leaders Circulated</h5> -->
                                    <!-- <p style="white-space: pre-line;"> -->
                                        <!-- {{ $newsItem->content_en }}
                                    </p>
                            </div>                             -->
                            <div class="article__text-block">
                                <!-- <h4 class='article__text-block-title'>50 Tech Leaders Circulated</h5> -->
                                    <p style="white-space: pre-line;">
                                        {{ $newsItem->content_ext_en}}
                                    </p>
                            </div>
                        </div>
                        <div class="spoiler" data-open="false">
                            <button class="spoiler-toggle article__tag" onclick="toggleSpoiler(event)">Redneck Style</button>
                            
                            <div class="spoiler-content">
                                <div class="article__text-block">
                                    <!-- <h4 class='article__text-block-title'>50 Tech Leaders Circulated</h5> -->
                                        <p style="white-space: pre-line;">
                                                {{ $newsItem->content_redneck_en }}
                                        </p>
                                </div>
                            </div>
                        </div>

                        <div class="article__tags">
                                @foreach($tags as $tag)
                                     <button class="article__tag">{{ $tag }}</button> 
                                @endforeach
                        </div>
                        <div class="article__footer">
                            <div class="article__footer-left">
                                <div class="article__logo">
                                    <picture>
                                        {{ $newsItem->source }}
                                        <!-- <source srcset="images/articles/logo.webp" type="image/webp"><img
                                            src="images/articles/logo.png" /> -->
                                    </picture>
                                </div>
                                <div class="article__data-info">
                                    <div class="article__data-info-item">
                                        <p>{{ $newsItem ->created_at->format('Y-m-d') }}</p>
                                    </div>
                                    <div class="article__data-info-item">
                                        <p>{{ $newsItem ->created_at->format('H:i') }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="article__footer-right">
                                <div class="article__data-info">
                                    <div class="article__data-info-item article__data-info-item--action"> <i
                                            class="ui_heart"></i>
                                        <p class="hearts">1877</p>
                                    </div>
                                    <div class="article__data-info-item article__data-info-item--action"> <i
                                            class="ui_comment"></i>
                                        <p>245</p>
                                    </div>
                                    <div class="article__data-info-item article__data-info-item--action"> <i
                                            class="ui_save"></i> </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </main>
    </div>
    <script src="/js/main.min.js"></script>
</body>

</html>