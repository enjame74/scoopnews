@php
  $mode = session()->get('mode', 'Highlights');
  $date = session()->get('date', 'All time');

  $filter = session()->get('filter', []);

  $search = session()->get('search', '');

  function formatTextIntoArray($text) {
    $formattedLine = preg_replace('/\s?\d+\./', '##', $text);;
    $lines = explode('##', $formattedLine);
    $formattedLines = [];

    foreach ($lines as $key => $line) {
        if($key == 0) {
            continue;
        }

        $formattedLine = preg_replace('/^\d+\./', '##', $line);
        $formattedLine = trim($formattedLine); // добавлено: удаление пробелов в начале строки
        if (!empty($formattedLine)) {
            $formattedLines[] = $formattedLine;
        }
    }

    return $formattedLines;
}

  function createNumberedList($lines)
  {
      $numberedList = '';

      foreach ($lines as $index => $line) {
          $numberedList .= ($index + 1) . '. ' . $line . PHP_EOL;
      }

      return $numberedList;
  }

  function getChunckedItems($arr) {
    $result = array();
    $count = count($arr);

    if(count($arr) >= 20) {
      for ($i = 0; $i < $count; $i += 10) {
        $tens = array_slice($arr, $i, 10);
        $elements = array_slice($tens, 0, 1);
        $result = array_merge($result, $elements);
      }
    } else {
      for ($i = 0; $i < $count; $i += 10) {
        $tens = array_slice($arr, $i, 10);
        $elements = array_slice($tens, 0, 3);
        $result = array_merge($result, $elements);
      }
    }
    
    if ($count <= 10 || count($result) < 3) {
      $result = array_slice($arr, 0, 3);
    }

    return array_slice($result, 0, 3);
  }
@endphp


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Scoop - news</title>
  <link rel="shortcut icon" href="/images/favicon/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon" href="/images/favicon/apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/apple-touch-icon-57x57.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/apple-touch-icon-72x72.png" />
  <link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/apple-touch-icon-76x76.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/apple-touch-icon-114x114.png" />
  <link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/apple-touch-icon-120x120.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/apple-touch-icon-144x144.png" />
  <link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/apple-touch-icon-152x152.png" />
  <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-touch-icon-180x180.png" />
  <link rel="preconnect" href="https://fonts.googleapis.com" />
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
  <link
    href="https://fonts.googleapis.com/css2?family=Manrope:wght@400;500;600;700&family=Unbounded:wght@700&family=Urbanist:wght@500;600;700;900&display=swap"
    rel="stylesheet" />
  <link rel="stylesheet" href="/css/style.min.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.4/jquery.min.js" integrity="sha512-pumBsjNRGGqkPzKHndZMaAG+bir374sORyzM3uulLV14lN5LyykqNk8eEeUlUkB3U0M4FApyaHraT65ihJhDpQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-infinitescroll/4.0.1/infinite-scroll.pkgd.min.js" integrity="sha512-R50vU2SAXi65G4oojvP1NIAC+0WsUN2s+AbkXqoX48WN/ysG3sJLiHuB1yBGTggOu8dl/w2qsm8x4Xr8D/6Hug==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NHN2837');</script>


<script>
    document.addEventListener('DOMContentLoaded', function() {
        var selectInputs = document.querySelectorAll('.selectModeDate .__select__input');

        selectInputs.forEach(function(selectInput) {
            selectInput.addEventListener('click', function() {
                var mode = this.getAttribute('data-date');
                if (mode) {
                    window.location.href = '/date?mode='  +  mode + '&url=' + '{{ $url }}';
                }
            });
        });
    });

    document.addEventListener('DOMContentLoaded', function() {
        var selectInputs = document.querySelectorAll('.selectModeMode .__select__input');

        selectInputs.forEach(function(selectInput) {
            selectInput.addEventListener('click', function() {
                var mode = this.getAttribute('data-mode');
                if (mode) {
                    window.location.href = '/mode?mode='  +  mode + '&url=' + '{{ $url }}';
                }
            });
        });
    });
</script>

</head>

<body>
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NHN2837"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <div class="news">
    <header class="news-header">
      <div class="news-header__container">

        <div class="news-header__container__inner">
          <div class="news-header__logo news-header__logo--desktop">
            <a href="/"><img src="/images/logo-text-shrink.svg" alt="logo" /></a>
          </div>

          <div class="news-header__logo news-header__logo--mobile">
            <a href="/"><img src="/images/logo-mobile.svg" alt="logo" /></a>
          </div>

        
          <form action="/search" method="GET">
          <div class="search-box">
              <div class="search-icon">
                <img src="/images/icons/search.png" alt="search">
              </div>
                <input type="hidden" name='url' value="{{ $url }}" style="display: none;">
                <input type="submit"  style="display: none;"/>
                <input type="text"  name="search" placeholder="Search..." value="{{ $search }}" class="search-input"/>
            </form>
            <div class="cancel-btn">
              <img src="/images/icons/close.svg" alt="close search">
            </div>
          </div>
        </div>
        
        <div class="header__menu-icon menu-burger-news">
          <span></span>
          <span></span>
          <span></span>
        </div>

        <div class="menu-burger-list">
          <div style="flex-grow: 1">
            <div class="avatar-box-burger">
              <div class="avatar">
                <style>
                  .circle2 {
                    max-height: 100%;
                    max-width: 100%;
                    border-radius: 50%;
                    font-family: roboto, arial;
                    color: white;
                    line-height: 60px;
                    text-align: center;
                    background: orange;
                    min-height: 100%;
                    min-width: 100%;
                  }
                </style>

                @if(auth()->user()?->image)
                  <img src="{{ auth()->user()?->image }}" alt="avatar" />
                @else
                  <div class="circle2">		
                    <script>
                      var initials = '{{ mb_substr(auth()->user()?->name, 0, 1) }}';
                      document.write(initials.toUpperCase()); 				// display in the html the value of the variable called 'initials' and capitalize them
                    </script>
                  </div>
                @endif
              </div>
              <p class="avatar-name">{{ auth()->user()?->name }}</p>
            </div>
            <div class="buttons-burger">
              <button class="buttons-burger__button buttons-burger__button--subscribtions link--open-moda-sub--custom"> Subscribtions</button>

              <script>
                document.addEventListener('DOMContentLoaded', function() {
                    var selectInputs = document.querySelectorAll('.link--open-moda-sub--custom');

                    selectInputs.forEach(function(selectInput) {
                        selectInput.addEventListener('click', function() {
                          const modal = document.querySelector(".modal-subscriptions");
                          modal.classList.add("modal--open");
                        });
                    });
                });
            </script>

              <script>
                document.addEventListener('DOMContentLoaded', function() {
                    var selectInputs = document.querySelectorAll('.buttons-burger__button--signOut');

                    selectInputs.forEach(function(selectInput) {
                        selectInput.addEventListener('click', function() {
                            var href = this.getAttribute('data-href');
                            if (href) {
                                window.location.href = href;
                            }
                        });
                    });
                });
            </script>

              <button class="buttons-burger__button buttons-burger__button--signOut" data-href="/logout">Sign Out</button>
            </div>
          </div>


          <footer class="footer footer-addPage">
            <div class="footer__inner">
              <div class="footer-top">
                <ul class="footer-social footer-social-mobile">
                  <li>
                    <a href="https://twitter.com/Scoop_Ai"> <img src="/images/landing/twitter.svg"></a>
                  </li>
                  <li>
                    <a href="https://www.instagram.com/scoop.ai"> <img src="/images/landing/inst.svg"></a>
                  </li>
                </ul>

                <div class="footer-top-top">
                  <ul class="footer-links">
                    <li><a href="/resources">Resources</a> </li>
                    <li><a href="/about">About</a> </li>
                  </ul>
                </div>
                <div class="footer-top-bottom">
                  <ul class="footer-links">
                    <li><a href="/policy">Privacy Policy</a> </li>
                    <li><a href="/terms">Terms of Use</a> </li>
                  </ul>

                  <ul class="footer-social footer-social-hide-mobile">
                    <li>
                      <a href="https://twitter.com/Scoop_Ai"> <img src="/images/landing/twitter.svg"></a>
                    </li>
                    <li>
                      <a href="https://www.instagram.com/scoop.ai"> <img src="/images/landing/inst.svg"></a>
                    </li>
                  </ul>
                </div>

              </div>
              <div class="footer-bottom">
                <p>© Scoop, 2023</p>
              </div>
            </div>
          </footer>
        </div>

        <div class="right-news-header">
          <ul class="news-header__menu">
            <li class="news-header__menu-item header-select">
              <form class="select-mode">
                <div class="__select selectMode selectModeMode" data-state="">
                  <div style="display: flex;align-items: center;" class="selectMode-open">
                  <div class="__select__title selectMode__title" data-default="{{ $mode }}">{{ $mode }} </div>
                    <img class="select-arrow" src="/images/icons/arrowDown.svg">
                  </div>
                  <div class="__select__content">
                      <input id="singleSelect1" data-mode="Highlights" class="__select__input" type="radio" name="singleSelect" @if($mode == 'Highlights') checked  @endif />
                      <label for="singleSelect1" class="__select__label">Highlights</label>
                      <input id="singleSelect0" data-mode="Statement" class="__select__input" type="radio" name="singleSelect" @if($mode == 'Statement') checked  @endif />
                      <label for="singleSelect0" class="__select__label">Statement</label>
                      <input id="singleSelect3" data-mode="Child mode" class="__select__input" type="radio" name="singleSelect" @if($mode == 'Child mode') checked  @endif  />
                      <label for="singleSelect3" class="__select__label">Child mode</label>
                      <input id="singleSelect4" data-mode="Summary" class="__select__input" type="radio" name="singleSelect" @if($mode == 'Summary') checked  @endif />
                      <label for="singleSelect4" class="__select__label">Summary</label>
                  </div>
                </div>
              </form>
            </li>
            <li class="news-header__menu-item header-select">
              <form class="select-mode">
                <div class="__select selectDate selectModeDate" data-state="">
                  <div style="display: flex;align-items: center;" class="selectDate-open">
                    <div class="__select__title selectDate__title" data-default="{{ $date }}">{{ $date }}</div>
                    <img class="select-arrow" src="/images/icons/arrowDown.svg">
                  </div>

                  <div class="__select__content selectDate_content">
                    <input id="selectDate1" data-date="Today" class="__select__input" type="radio" name="selectDate" @if($date == 'Today') checked  @endif/>
                    <label for="selectDate1" class="__select__label selectDate_labels">Today</label>
                    <input id="selectDate0" data-date="Week" class="__select__input" type="radio" name="selectDate" @if($date == 'Week') checked  @endif/>
                    <label for="selectDate0" class="__select__label selectDate_labels">Week</label>
                    <input id="selectDate3" data-date="Month" class="__select__input" type="radio" name="selectDate" @if($date == 'Month') checked  @endif />
                    <label for="selectDate3" class="__select__label selectDate_labels">Month</label>
                    <input id="selectDate4" data-date="All Time" class="__select__input" type="radio" name="selectDate" @if($date == 'All Time') checked  @endif  />
                    <label for="selectDate4" class="__select__label selectDate_labels">All time</label>
                  </div>
                </div>
              </form>

            </li>
            <li class="news-header__menu-item link--open-modal">Topics</li>
            <li class="news-header__menu-item link--open-modal-subscriptions">Subscriptions</li>
          </ul>
          <div class="avatar-box">
            <div class="avatar">
              <style>
                .circle {
                  max-height: 100%;
                  max-width: 100%;
                  border-radius: 50%;
                  font-family: roboto, arial;
                  color: white;
                  line-height: 22px;
                  text-align: center;
                  background: orange;
                }
              </style>

              @if(auth()->user()?->image)
                <img src="{{ auth()->user()?->image }}" alt="avatar" />
              @else
                <div class="circle">		
                  <script>
                    var initials = '{{ mb_substr(auth()->user()?->name, 0, 1) }}';
                    document.write(initials.toUpperCase()); 				// display in the html the value of the variable called 'initials' and capitalize them
                  </script>
                </div>
              @endif
            </div>
            <p class="avatar-name">{{ auth()->user()?->name }}</p>
            <script>
                document.addEventListener('DOMContentLoaded', function() {
                    var selectInputs = document.querySelectorAll('.avatar-btn-logOut');

                    selectInputs.forEach(function(selectInput) {
                        selectInput.addEventListener('click', function() {
                            var href = this.getAttribute('data-href');
                            if (href) {
                                window.location.href = href;
                            }
                        });
                    });
                });
            </script>
            <button class="avatar-btn-logOut" data-href="/logout"><img src="/images/news/logOut.svg" class='sign-out-img' /> Sign
              Out</button>
          </div>
        </div>
      </div>
    </header>

    <main class="news-main">
      <div class="news-container">
      <aside class="news-aside">
            <div class="news-style-img">
              @if($mode == 'Highlights')
              <img src="/images/news/highlights.png" id="news-mode-img" />
              @elseif($mode == 'Child mode')
              <img src="/images/news/child.png" id="news-mode-img" />
              @elseif($mode == 'Summary')
              <img src="/images/news/summary.png" id="news-mode-img" />
              @else
              <img src="/images/news/statement.png" id="news-mode-img"/>
              @endif
            </div>
          <div class="aside-bottom">
            <div class="select-topics-mobile @if($filter) select-topics-mobile--selected @endif link--open-modal">
              <p class="link--open-modal--custom">Select topics</p>
              <script>
                document.addEventListener('DOMContentLoaded', function() {
                    var selectInputs = document.querySelectorAll('.link--open-modal--custom');

                    selectInputs.forEach(function(selectInput) {
                        selectInput.addEventListener('click', function() {
                          const modal = document.querySelector(".modal-topics");
                          modal.classList.add("modal--open");
                        });
                    });
                });
            </script>
            </div>
            <form class="select-mode select-mobile">
              <div class="__select selectMode selectMode-mobile selectModeMode" data-state="">
                <div style="display: flex;align-items: center;" class="selectMode-open select-open-mobile">
                <div class="__select__title selectMode__title" data-default="{{ $mode }}">{{ $mode }} </div>
                  <div style="display: flex; align-items: center;">
                    <img class="select-arrow" src="/images/icons/arrowDown.svg">
                  </div>
                </div>
                <div class="__select__content">
                    <input id="singleSelect1" data-mode="Highlights" class="__select__input" type="radio" name="singleSelect" @if($mode == 'Highlights') checked  @endif />
                    <label for="singleSelect1" class="__select__label">Highlights</label>
                    <input id="singleSelect0" data-mode="Statement" class="__select__input" type="radio" name="singleSelect" @if($mode == 'Statement') checked  @endif />
                    <label for="singleSelect0" class="__select__label">Statement</label>
                    <input id="singleSelect3" data-mode="Child mode" class="__select__input" type="radio" name="singleSelect" @if($mode == 'Child mode') checked  @endif  />
                    <label for="singleSelect3" class="__select__label">Child mode</label>
                    <input id="singleSelect4" data-mode="Summary" class="__select__input" type="radio" name="singleSelect" @if($mode == 'Summary') checked  @endif />
                    <label for="singleSelect4" class="__select__label">Summary</label>
                </div>
              </div>
            </form>


            <form class="select-mode select-mobile">
              <div class="__select selectDate selectDate-mobile selectModeDate" data-state="">
                <div style="display: flex;align-items: center;" class="selectDate-open select-open-mobile">
                  <div class="__select__title selectDate__title" data-default="{{ $date }}">{{ $date }}</div>
                  <div style="display: flex; align-items: center;">
                    <img class="select-arrow" src="/images/icons/arrowDown.svg">
                  </div>
                </div>
                <div class="__select__content selectDate_content">
                    <input id="selectDate1" data-date="Today" class="__select__input" type="radio" name="selectDate" @if($date == 'Today') checked  @endif/>
                    <label for="selectDate1" class="__select__label selectDate_labels">Today</label>
                    <input id="selectDate0" data-date="Week" class="__select__input" type="radio" name="selectDate" @if($date == 'Week') checked  @endif/>
                    <label for="selectDate0" class="__select__label selectDate_labels">Week</label>
                    <input id="selectDate3" data-date="Month" class="__select__input" type="radio" name="selectDate" @if($date == 'Month') checked  @endif />
                    <label for="selectDate3" class="__select__label selectDate_labels">Month</label>
                    <input id="selectDate4" data-date="All Time" class="__select__input" type="radio" name="selectDate" @if($date == 'All Time') checked  @endif  />
                    <label for="selectDate4" class="__select__label selectDate_labels">All time</label>
                  </div>
              </div>
            </form>

            <div class="news-aside-desktop">
              <ul class="aside__menu">
                <li><a href="/resources">Resources</a></li>
                <li> <a href="/about">About</a></li>
              </ul>
              <ul class="aside__menu">
                <li><a href="/policy">Privacy Policy</a> </li>
                <li><a href="/terms">Terms of Use</a> </li>
              </ul>
              <p class="right">© Scoop, 2023</p>
            </div>

          </div>
        </aside>

        <div class="news-news">
          @if($mode == 'Highlights')
          <p class="news-title" id="news-title">
            Welcome, seekers of truth and discovery!
          </p>
          @elseif($mode == 'Child mode')
          <p class="news-title" id="news-title">
              Hello, young explorers of news and wonder!
          </p>
          @elseif($mode == 'Summary')
          <p class="news-title" id="news-title">
            Greetings, seekers of wisdom and news!
          </p>
          @else
          <p class="news-title" id="news-title">
            Greetings, citizens of knowledge and insight!
          </p>
          @endif
          <div class="news-list">
          @foreach($news as $newsItem)
            <div class="news-item">
              <div class="news-item__logo">
              @if($newsItem->source == 'Medium')
                <img src="/images/news/logos/medium.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'ESPN')
                <img src="/images/news/logos/espn.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'BBC News')
                <img src="/images/news/logos/bbc-news.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Reddit /r/all')
                <img src="/images/news/logos/reddit.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'TechCrunch')
                <img src="/images/news/logos/tech-crunch.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Tech Crunch')
                <img src="/images/news/logos/tech-crunch.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Newsweek')
                <img src="/images/news/logos/newsweek.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'National Geographic')
                <img src="/images/news/logos/national-geographic.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Independent')
                <img src="/images/news/logos/independent.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'National Review')
                <img src="/images/news/logos/national-review.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'CBC News')
                <img src="/images/news/logos/cbc.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Engadget')
                <img src="/images/news/logos/engodget.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Hacker News')
                <img src="/images/news/logos/hacker-news.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Buzzfeed')
                <img src="/images/news/logos/buzz-feed.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Fortune')
                <img src="/images/news/logos/fortune.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'CNN')
                <img src="/images/news/logos/cnn.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Associated Press')
                <img src="/images/news/logos/ap.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Reuters')
                <img src="/images/news/logos/reuters.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'The Huffington Post')
                <img src="/images/news/logos/huffpost.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Bild')
                <img src="/images/news/logos/bild.png" alt="logo-newspaper" />
                @elseif($newsItem->source == 'TheHustle')
                <img src="/images/news/logos/hustle.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Insider')
                <img src="/images/news/logos/insider.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Bloomberg')
                <img src="/images/news/logos/bloomberg.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Business Insider')
                <img src="/images/news/logos/business-insider.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Mashable')
                <img src="/images/news/logos/mashable.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'New York Magazine')
                <img src="/images/news/logos/newyork.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Politico')
                <img src="/images/news/logos/politico.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'TechRadar')
                <img src="/images/news/logos/techradar.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Tech Radar')
                <img src="/images/news/logos/techradar.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'The Verge')
                <img src="/images/news/logos/theverge.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Time')
                <img src="/images/news/logos/time.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'Wired')
                <img src="/images/news/logos/wired.svg" alt="logo-newspaper" />
                @elseif($newsItem->source == 'WSJ')
                <img src="/images/news/logos/wsj.svg" alt="logo-newspaper" />
                @else
                  <img src="/images/news/logos/default.svg" alt="logo-newspaper" />
                @endif
              </div>

              <div class="news-item__info">
                <div class="news-item__header">
                  <div style="display: flex; gap: 8px; align-items: center; align-self: flex-start;">
                    <div class="news-item__logo--mobile">
                    @if($newsItem->source == 'Medium')
                    <img src="/images/news/logos/medium.svg" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'ESPN')
                    <img src="/images/news/logos/espn.png" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'BBC News')
                    <img src="/images/news/logos/bbc-news.png" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'Reddit /r/all')
                    <img src="/images/news/logos/reddit.png" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'TechCrunch')
                    <img src="/images/news/logos/tech-crunch.svg" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'Tech Crunch')
                    <img src="/images/news/logos/tech-crunch.svg" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'Newsweek')
                    <img src="/images/news/logos/newsweek.png" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'National Geographic')
                    <img src="/images/news/logos/national-geographic.png" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'Independent')
                    <img src="/images/news/logos/independent.png" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'National Review')
                    <img src="/images/news/logos/national-review.png" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'CBC News')
                    <img src="/images/news/logos/cbc.png" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'Engadget')
                    <img src="/images/news/logos/engodget.png" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'Hacker News')
                    <img src="/images/news/logos/hacker-news.png" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'Buzzfeed')
                    <img src="/images/news/logos/buzz-feed.png" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'Fortune')
                    <img src="/images/news/logos/fortune.png" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'CNN')
                    <img src="/images/news/logos/cnn.png" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'Associated Press')
                    <img src="/images/news/logos/ap.png" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'Reuters')
                    <img src="/images/news/logos/reuters.png" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'The Huffington Post')
                    <img src="/images/news/logos/huffpost.png" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'Bild')
                    <img src="/images/news/logos/bild.png" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'TheHustle')
                    <img src="/images/news/logos/hustle.svg" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'Insider')
                    <img src="/images/news/logos/insider.svg" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'Bloomberg')
                    <img src="/images/news/logos/bloomberg.svg" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'Business Insider')
                    <img src="/images/news/logos/business-insider.svg" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'Mashable')
                    <img src="/images/news/logos/mashable.svg" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'New York Magazine')
                    <img src="/images/news/logos/newyork.svg" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'Politico')
                    <img src="/images/news/logos/politico.svg" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'TechRadar')
                    <img src="/images/news/logos/techradar.svg" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'Tech Radar')
                    <img src="/images/news/logos/techradar.svg" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'The Verge')
                    <img src="/images/news/logos/theverge.svg" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'Time')
                    <img src="/images/news/logos/time.svg" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'Wired')
                    <img src="/images/news/logos/wired.svg" alt="logo-newspaper" />
                    @elseif($newsItem->source == 'WSJ')
                    <img src="/images/news/logos/wsj.svg" alt="logo-newspaper" />
                    @else
                      <img src="/images/news/logos/default.svg" alt="logo-newspaper" />
                    @endif
                    </div>
                    <h3 class="news-item__name">{{ $newsItem->source }}</h3>
                  </div>

                  <div class="news-item__data">
                    <p>{{ $newsItem ->created_at->format('Y-m-d') }}</p>
                    <div class="separator"></div>
                    <p>{{ $newsItem ->created_at->format('H:i') }}</p>
                  </div>
                </div>
                <ul class="news-item__tags">
                  @php
                      $newsTags = $newsItem->tags;
                      $allTags = [];

                      foreach($newsTags as $newsTag) {
                      $name = $newsTag->name;

                      if (strlen($name) > 3 && strlen($name) < 15) { $allTags[]=$name; } } $newsTags=array_slice(array_unique($allTags),
                        0, 7); $otherCount=count($allTags) - 3; @endphp
                     @foreach($newsTags as $tag) <li><a href="/news/tags/{{ $tag }}">#{{ $tag }}</a></li> @endforeach
                </ul>
                <div class="news-item__text">
                  <p>
                    {{ $newsItem->name }}
                  </p>
      
                    @if($mode == 'Highlights')
                      @php
                        $list = createNumberedList(getChunckedItems(formatTextIntoArray($newsItem->content_quote_en)));
                      @endphp
                    <p style="white-space: pre-line;">{{ $list }}</p>
                    @elseif($mode == 'Child mode')
                    <p>{{ $newsItem->content_redneck_en }}</p>
                    @elseif($mode == 'Summary')
                    <p>{{ $newsItem->content_ext_en }}</p>
                    @else
                    <p>{{ $newsItem->content_short_en }}</p>
                    @endif
                </div>
                <div class="news-item__footer">
                  <div class="news-item__footer-left">
                    <div class="news-item__footer-item">
                      <img src="/images/news/save.svg">
                      <p>{{ rand(100, 2000) }}</p>
                    </div>
                    <div class="separator"></div>
                    <div class="news-item__footer-item">
                      <img src="/images/news/share.svg">
                      <p>{{ rand(100, 2000) }}</p>
                    </div>
                  </div>
                  <a target="_blank" href="{{ $newsItem->source_url }}" class="sourse-link">Source <img src="/images/news/sourse.svg"></a>
                </div>
              </div>
            </div>
            
            @endforeach
            <script type="text/javascript">
                var setHeader = function (xhr) {
                  xhr.setRequestHeader('Content-Type', 'application/json');
                  xhr.setRequestHeader('Accept', 'application/json');
                }

                $.ajaxSetup({
                  beforeSend: setHeader
                });

                $('.news-list').infiniteScroll({
                  // options
                  path: function() {
                    return '{{ env('APP_URL') }}{{ $loaderUri }}?page=' + (this.pageIndex + 1);
                  },
                  append: '.news-item',
                  history: false,
                });
            </script>
          </div>
        </div>

        <div class="news-social">
          <ul class="footer-social">
            <li>
              <a href="https://twitter.com/Scoop_Ai"> <img src="/images/landing/twitter.svg"></a>
            </li>
            <li>
              <a href="https://www.instagram.com/scoop.ai"> <img src="/images/landing/inst.svg"></a>
            </li>
          </ul>
        </div>

        <div class="news-mobile-footer">
          <ul class="aside__menu">
            <a href="/policy"><li>Privacy Policy</li></a>
            <a href="/terms"><li>Terms of Use</li></a>
          </ul>
          <p class="right">© Scoop, 2023</p>
        </div>
      </div>
    </main>
  </div>

    <div class="modal modal-topics">
      <div class="modal-outer">
        <button type="button" class="btn--close-modal btn--close-modal-topics">
          <svg width="24" height="24" viewBox="0 0 24 24">
            <rect width="24" height="24" rx="12" />
            <path
              d="M3 12C3 7.02944 7.02944 3 12 3V3C16.9706 3 21 7.02944 21 12V12C21 16.9706 16.9706 21 12 21V21C7.02944 21 3 16.9706 3 12V12Z" />
            <path fill-rule="evenodd" clip-rule="evenodd"
              d="M6.7045 15.7201C6.26517 16.1594 6.26517 16.8717 6.7045 17.311C7.14384 17.7504 7.85616 17.7504 8.2955 17.311L12.0533 13.5533L15.8111 17.3111C16.2504 17.7504 16.9628 17.7504 17.4021 17.3111C17.8414 16.8717 17.8414 16.1594 17.4021 15.7201L13.6443 11.9623L17.3111 8.29543C17.7504 7.85609 17.7504 7.14378 17.3111 6.70444C16.8718 6.2651 16.1595 6.2651 15.7201 6.70444L12.0533 10.3713L8.38649 6.70448C7.94715 6.26514 7.23483 6.26514 6.7955 6.70448C6.35616 7.14382 6.35616 7.85613 6.7955 8.29547L10.4623 11.9623L6.7045 15.7201Z"
              fill="#ffffff" />
          </svg>
        </button>
        <div class="modal-wrapper">
          <div class="modal-inner">
            <div class="modal-header">
              <h4 class="modal-title">Topics</h4>
              <button class="clear-all-btn">Clear all</button>
            </div>
                <div class="accordion">
                  @foreach($tags as $key=> $topic)
                        @if($key == 'Other')
                          @continue;
                        @endif
                    <div class="accordion-item">
                      @php
                        $toogle = 'false';
                        $isAll = false;
                        $selectedTags = [];

                        if(isset($filter[ucfirst($key)])) {
                          $toogle = 'true';
                          $isAll = ($filter[ucfirst($key)] == 'all') ? true : false;

                          if($isAll) {
                            $selectedTags = [];
                          } else {
                            $selectedTags = $filter[ucfirst($key)];
                          }
                        }

                      @endphp
                      <button  class="accordion-expand-btn" id="accordion-button-1" aria-expanded="{{ $toogle }}"><span
                          class="accordion-title">{{ ucfirst($key) }}</span><span class="icon" aria-hidden="true"></span></button>
                      <div class="accordion-content">
                        <div class="tab-filter__buttons">
                          <button id="{{ ucfirst($key) }}" data-topic="{{ ucfirst($key) }}" class="tab-filter__button select-all @if($isAll) tab-filter__button--active @endif">Select All</button>

                          @php
                            $tagList = [];

                            foreach($topic as $tagItem) {
                            $name = $tagItem->name;

                            if (strlen($name) > 3 && strlen($name) < 15) { $tagList[]=$tagItem; } }
                              $tagList=array_slice($tagList, 0, 20); 
                          @endphp 
                          @foreach($tagList as $item) 
                            <button  data-topic="{{ ucfirst($key) }}" data-tag="{{ $item->name }}" class="tab-filter__button tags-select {{ ucfirst($key) }}Select @if(in_array($item->name, $selectedTags)) tab-filter__button--active @endif">#{{ $item->name }}</button>
                          @endforeach
                        </div>
                      </div>
                    </div>
                  @endforeach
              
                <button type="submit" class="modal-apply-btn topics-modal-apply-btn">Apply</button>
                <script>
                    document.addEventListener('DOMContentLoaded', function() {
                        var selectInputs = document.querySelectorAll('.select-all');

                        selectInputs.forEach(function(selectInput) {
                            selectInput.addEventListener('click', function(event) {
                              const element = event.srcElement;
                              element.classList.add("tab-filter__button--active");
                              topic = element.getAttribute('data-topic')
                              var tags = document.querySelectorAll('.' + topic +'Select');

                              tags.forEach(function(tag) {
                                tag.classList.remove("tab-filter__button--active");
                              });
                            });
                        });

                        var tagsInputs = document.querySelectorAll('.tags-select');

                        tagsInputs.forEach(function(tagInput) {
                          tagInput.addEventListener('click', function(event) {
                              const element = event.srcElement;
                              const type = document.getElementById(element.getAttribute('data-topic'))
                              type.classList.remove("tab-filter__button--active");
                            });
                        });
                    });
                  // select-all
                      document.addEventListener('DOMContentLoaded', function() {
                            var clearSelects = document.querySelectorAll('.clear-all-btn');
                            clearSelects.forEach(function(clearSelect) {
                              clearSelect.addEventListener('click', function(event) {
                                var selectInputs = document.querySelectorAll('.tab-filter__button--active');

                                selectInputs.forEach(function(selectInput) {
                                      selectInput.classList.remove("tab-filter__button--active");
                                });
                              });
                            });
                        })

                     document.addEventListener('DOMContentLoaded', function() {
                        var selectInputs = document.querySelectorAll('.topics-modal-apply-btn');

                        selectInputs.forEach((selectInput) =>{
                            selectInput.addEventListener('click', function() {
                              var filter = {};

                              const selects = document.querySelectorAll(".tab-filter__button--active");

                              selects.forEach(function (select) {
                                const topic = select.getAttribute('data-topic')
                                const tag = select.getAttribute('data-tag')

                                var prev = filter[topic] || []

                                if (tag) {
                                  filter[topic] = [
                                    ...prev,
                                    tag
                                  ]
                                } else {
                                  filter[topic] = 'all'
                                }
                              })
                              
                              const post = JSON.stringify({
                                filter: filter,
                              })
                              const url = "/filter"
                              const xhr = new XMLHttpRequest()
                              xhr.open('POST', url, true)
                              xhr.setRequestHeader('Content-type', 'application/json; charset=UTF-8')
                              xhr.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}");
                              xhr.send(post);
                              xhr.onload = function () {
                                window.location = '/news'
                              }
                            });
                        });
                    });
                </script>

            </div>
          </div>

        </div>
      </div>
    </div>



    <div class="modal modal-subscriptions">
      <div class="modal-outer">
        <button type="button" class="btn--close-modal-subscriptions btn--close-modal">
          <svg width="24" height="24" viewBox="0 0 24 24">
            <rect width="24" height="24" rx="12" />
            <path
              d="M3 12C3 7.02944 7.02944 3 12 3V3C16.9706 3 21 7.02944 21 12V12C21 16.9706 16.9706 21 12 21V21C7.02944 21 3 16.9706 3 12V12Z" />
            <path fill-rule="evenodd" clip-rule="evenodd"
              d="M6.7045 15.7201C6.26517 16.1594 6.26517 16.8717 6.7045 17.311C7.14384 17.7504 7.85616 17.7504 8.2955 17.311L12.0533 13.5533L15.8111 17.3111C16.2504 17.7504 16.9628 17.7504 17.4021 17.3111C17.8414 16.8717 17.8414 16.1594 17.4021 15.7201L13.6443 11.9623L17.3111 8.29543C17.7504 7.85609 17.7504 7.14378 17.3111 6.70444C16.8718 6.2651 16.1595 6.2651 15.7201 6.70444L12.0533 10.3713L8.38649 6.70448C7.94715 6.26514 7.23483 6.26514 6.7955 6.70448C6.35616 7.14382 6.35616 7.85613 6.7955 8.29547L10.4623 11.9623L6.7045 15.7201Z"
              fill="#ffffff" />
          </svg>
        </button>
        <div class="modal-wrapper">
          <div class="modal-inner">
            <div class="modal-header">
              <h4 class="modal-title">Subscribtions to topics</h4>
            </div>
        <div class="subscribtions-disabled">
          <img src="/images/news/dont-panic.svg">
          <p>Subscription functionality will soon be available</p>
        </div>
            <div class="modal-subscription-footer">
              <div class="modal-subscription-footer-left">
                <form>
                  <label class="send-to-email">
                    <input type="checkbox" class="checkbox" disabled>
                    Send it to email
                  </label>
                </form>

                <form class="select-mode">
                  <div class=" selectMode-modal" data-state="">
                    <div style="display: flex;align-items: center;" class="selectMode-open">
                      <div class="__select__title selectMode__title" data-default="Highlights">Highlights </div>
                      <img class="select-arrow" src="/images/icons/arrowDown.svg">
                    </div>
                    <div class="__select__content">
                      <input id="singleSelect1" class="__select__input" type="radio" name="singleSelect" checked />
                      <label for="singleSelect1" class="__select__label selectMode__label">Highlights</label>
                      <input id="singleSelect0" class="__select__input" type="radio" name="singleSelect" />
                      <label for="singleSelect0" class="__select__label selectMode__label">Summary</label>
                      <input id="singleSelect3" class="__select__input" type="radio" name="singleSelect" />
                      <label for="singleSelect3" class="__select__label selectMode__label">Statement</label>
                      <input id="singleSelect4" class="__select__input" type="radio" name="singleSelect" />
                      <label for="singleSelect4" class="__select__label selectMode__label">Child</label>
                    </div>
                  </div>
                </form>
              </div>
              <button class="modal-subscription-btn">Save</button>
            </div>
          </div>

        </div>
      </div>
    </div>
  <script src="/js/main.min.js"></script>
</body>

</html>