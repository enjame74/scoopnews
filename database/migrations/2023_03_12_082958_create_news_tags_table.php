<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('news_tags', function (Blueprint $table) {
            $table->integer('news_id')->unsigned()->index();            
            $table->integer('tags_id')->unsigned()->index();

            $table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');
            $table->foreign('tags_id')->references('id')->on('tags');

            $table->primary(['news_id', 'tags_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('news_tags', function (Blueprint $table) {
            //
        });
    }
};
