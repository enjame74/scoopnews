<?php

use Illuminate\Support\Facades\Route;

use  App\Http\Controllers\NewsController;
use App\Http\Controllers\Auth\LoginController;
use Laravel\Socialite\Facades\Socialite;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function() {
    return view('home');
});


 
Route::get('/auth/google', function () {
    return Socialite::driver('google')->redirect();
});
 
Route::get('/auth/callback', function () {
    $googleUser = Socialite::driver('google')->user();
 
    $user = \App\Models\User::updateOrCreate([
        'email' => $googleUser->email,
    ], [
        'name' => $googleUser->name,
        'email' => $googleUser->email,
        'image' => $googleUser->avatar,
    ]);
 
    Auth::login($user, true);
 
    return redirect('/news');
});

Route::middleware('auth')->get('/mode', [NewsController::class, 'mode']);
Route::middleware('auth')->get('/date', [NewsController::class, 'date']);
Route::middleware('auth')->get('/search', [NewsController::class, 'search']);
Route::middleware('auth')->post('/filter', [NewsController::class, 'filter']);

Route::middleware('auth')->get('/news', [NewsController::class, 'index']);

Route::get('/debug', [NewsController::class, 'debug']);

// Route::middleware('auth')->get('/news/{id}', [NewsController::class, 'show']);
Route::middleware('auth')->get('/news/loadData', [NewsController::class, 'loadData']);
Route::middleware('auth')->get('/news/category/{category}', [NewsController::class, 'showByCategory']);
Route::middleware('auth')->get('/news/category/{category}/loadData', [NewsController::class, 'loadDataByCategory']);
Route::middleware('auth')->get('/news/tags/{tag}', [NewsController::class, 'showByTag']);
Route::middleware('auth')->get('/news/tags/{tag}/loadData', [NewsController::class, 'loadDataByTag']);

Route::middleware('auth')->get('/logout', [LoginController::class, 'logout']);

Route::get("/about", function(){
    return view("about");
});

Route::get("/terms", function(){
    return view("terms");
});

Route::get("/policy", function(){
    return view("policy");
});

Route::get("/resources", function(){
    return view("resources");
});


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
