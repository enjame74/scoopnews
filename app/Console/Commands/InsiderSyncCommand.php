<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Arr;
use App\Models\News;
use App\Jobs\ProcessNewsJob;
use Symfony\Component\DomCrawler\Crawler;
use Illuminate\Support\Carbon;
use GuzzleHttp\Client;

class InsiderSyncCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:insider';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start of sync command';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $categories = ['news'];

        foreach($categories as $category) {
            $page = 1; // начинаем с первой страницы

            $parsedItems = [];

            $client = new Client();

            do {
                $url = "https://www.insider.com/{$category}?page={$page}";
                $response = $client->get($url);
                $html = $response->getBody()->getContents();
                $crawler = new Crawler($html);
            
                // Извлекаем ссылки на новости
                $newsLinks = $crawler->filter('.fusion-post-wrapper a')->each(function (Crawler $node) {
                    return $node->link()->getUri();
                });
            
                // Обрабатываем каждую новость
                foreach ($newsLinks as $newsLink) {
                    $response = $client->get($newsLink);
                    $html = $response->getBody()->getContents();
                    $newsCrawler = new Crawler($html);
            
                    $title = $newsCrawler->filter('.post-title')->text();
                    $content = $newsCrawler->filter('.fusion-post-content')->html();
                    $date = $newsCrawler->filter('.fusion-meta-info-wrapper .fusion-inline_date')->attr('datetime');
                    $imageNode = $newsCrawler->filter('.fusion-rollover-img img');
                    $image = $imageNode->count() ? $imageNode->attr('src') : null;
            
                    $parsedItems[] = [
                        'title' => $title,
                        'content' => $content,
                        'link' => $newsLink,
                        'category' => $category,
                        'date' => $date,
                        'image' => $image,
                    ];

                    dd($parsedItems);
                }
            
                // Увеличиваем номер страницы
                $page++;
            
            } while (count($newsLinks) > 0); 
        }
        
        return [];
    }

    function parseArticleContent($url)
    {
        $response = Http::get($url);

        if ($response->successful()) {
            $html = $response->body();
            $crawler = new Crawler($html);

            $content = $crawler->filter('.post-content')->text();

            return $content;
        }

        return '';
    }


    function saveNewsItems($parsedItems)
    {
        foreach ($parsedItems as $item) {
            News::create([
                'name' => Arr::get($item, 'title', ''),
                'content_en' => Arr::get($item, 'content', ''),
                'source_url' => Arr::get($item, 'link', ''),
                'source' => 'Insider',
                'topic' => Arr::get($item, 'category', ''),
                'preview' => Arr::get($item, 'image', ''),
                'created_at' => Carbon::parse(Arr::get($item, 'date', Carbon::now())),
                'lang' => 'en',
            ]);
        }
    }

    protected function urljoin($base, $relative)
    {
        if (parse_url($relative, PHP_URL_SCHEME) !== null) {
            return $relative;
        }

        $parts = parse_url($base);

        if ($relative[0] == '/') {
            return $parts['scheme'] . '://' . $parts['host'] . $relative;
        }

        $path = isset($parts['path']) ? preg_replace('#/[^/]*$#', '', $parts['path']) : '';

        while (substr($relative, 0, 3) === '../') {
            $relative = substr($relative, 3);
            $path = preg_replace('#/[^/]*$#', '', $path);
        }

        return $parts['scheme'] . '://' . $parts['host'] . $path . '/' . $relative;
    }
}
