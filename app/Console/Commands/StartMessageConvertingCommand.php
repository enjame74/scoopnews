<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Arr;
use App\Models\News;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\Response;

class StartMessageConvertingCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gpt:message';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start of gpt command';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $newsList = News::where('is_processed', false)->get();

        $apiKey = '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';
        $url = 'https://api.openai.com/v1/engines/text-davinci-003/completions';

        foreach ($newsList as $news) {
                // Получение текста сообщения из формы или другого источника
            $message_text = '
            Владимир St, [30.03.2023 15:30]
            Щербакова 74)

            Георгий Бодров, [30.03.2023 15:32]
            Оки, я буду в районе 18:30

            Георгий Бодров, [30.03.2023 16:10]


            Георгий Бодров, [30.03.2023 16:11]
            Писец))

            Владимир St, [30.03.2023 16:11]
            ёьаны рот

            Владимир St, [30.03.2023 16:11]
            этож прям как в другой город))

            Георгий Бодров, [30.03.2023 16:23]
            Так да) я чет не думал что ты там далеко 😅

            Владимир St, [30.03.2023 16:40]
            ну относительно меня ты тоже не близко :D

            Георгий Бодров, [30.03.2023 17:04]
            Я на месте

            Владимир St, [30.03.2023 17:05]
            вылетаю

            Георгий Бодров, [30.03.2023 17:05]
            Куда ты вылетаешь? во сколько приземлишься?
            И как тебе игра нашей сборной по футболу?
            ';

            // Разбиение сообщения на части по 2048 токенов
            $message_parts = $this->splitTextIntoChunks($this->removeExtraSpacesAndLineBreaks($message_text), 3900);

            // // Отправка запросов к ChatGPT API по частям сообщения
            // sleep(20);

            $quotes = '';
            foreach ($message_parts as $part) {
                if($part == '') {
                    continue;
                } 

                $request_count = 0;
                $request_failed = false;

                do {
                    // Отправка запроса к ChatGPT API
                    $response = Http::retry(3, 20000)->withHeaders([
                        'Content-Type' => 'application/json',
                        'Authorization' => "Bearer $apiKey",
                    ])->timeout(500)->post('https://api.openai.com/v1/engines/text-davinci-003/completions', [
                        'prompt' => 'Сгенерируй ответ в шуточной форме и в дворовом стиле разговора на следующий диалог от лица Владимира' . $part,
                        'max_tokens' => 3224,
                        'temperature' => 1,
                        'stop' => ['\n'],
                    ]);

                    // Обработка ошибок
                    if ($response->failed()) {
                        dump($response->json('error'));
                        $request_failed = true;
                        $request_count++;
                        sleep(2 * $request_count);
                        continue;
                    }

                    $response_data = $response->json();
                    $quotes .= $response_data['choices'][0]['text'];

                    $request_failed = false;

                } while ($request_failed && $request_count < 3);
            }

            dd($quotes);
        }
    }

    protected function splitTextIntoChunks($text, $maxChunkLength) {
        $lines = preg_split('/(\.|\n)/', $text);
        $chunks = [];
        $currentChunk = '';
    
        foreach ($lines as $key => $line) {
            if($key == 0) {
                continue;
            }
            
            if (strlen($currentChunk) + strlen($line) + 1 <= $maxChunkLength) {
                $currentChunk .= $line . PHP_EOL;
            } else {
                $chunks[] = $currentChunk;
                $currentChunk = $line . PHP_EOL;
            }
        }
    
        if (!empty($currentChunk)) {
            $chunks[] = $currentChunk;
        }
    
        return $chunks;
    }

    protected function formatTextIntoArray($text) {
        $lines = explode(PHP_EOL, $text);
        $formattedLines = [];
    
        foreach ($lines as $key => $line) {
            if($key == 0) {
                continue;
            }

            $formattedLine = preg_replace('/^\d+\./', '', $line);
            $formattedLine = trim($formattedLine); // добавлено: удаление пробелов в начале строки
            if (!empty($formattedLine)) {
                $formattedLines[] = $formattedLine;
            }
        }
    
        return $formattedLines;
    }
    
    protected function createNumberedList($lines)
    {
        $numberedList = '';

        foreach ($lines as $index => $line) {
            $numberedList .= ($index + 1) . '. ' . $line . PHP_EOL;
        }

        return $numberedList;
    }

    protected function removeExtraSpacesAndLineBreaks($text) {
        $text = preg_replace('/\s+/', ' ', $text); // Заменяем все пробельные символы на один пробел
        $text = preg_replace('/[\r\n]+/', PHP_EOL, $text); // Заменяем все подряд идущие символы переноса строки на один символ переноса
        $text = trim($text); // Удаляем пробелы и символы переноса строки с начала и конца текста
        return $text;
    }
}
