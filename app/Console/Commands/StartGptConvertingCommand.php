<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Models\News;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\Response;

class StartGptConvertingCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gpt:converting';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start of gpt command';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $newsList = News::where('is_processed', false)->orderBy('created_at', 'DESC')->take(5000)->get();

        $apiKey = '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';
        // $apiKey = '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';

        $url = 'https://api.openai.com/v1/engines/text-davinci-003/completions';

        foreach ($newsList as $news) {
                // Получение текста сообщения из формы или другого источника
            $message_text = $news->content_en;
            
            if(strlen($message_text) > 2000) {
                $message_text = Str::substr($message_text, 0, 1200);
            }

            $chunk = $news->name . PHP_EOL . Str::substr($news->content_en, 0 , 512);

            $topic = 'Other';

            $needTopic = [
                'Politics',
                'Business',
                'Tech',
                'Science',
                'Cybersecurity',
                'AI',
                'Crypto',
                'Gadgets',
                'Sports',
                'Lifestyle',
                'Culture',
                'Entertainment',
            ];

            $topics = join(", ", $needTopic);
            
            $request_failed = false;
            do {
                // Отправка запроса к ChatGPT API
                try {
                    $response = Http::retry(3, 20000)->withHeaders([
                        'Content-Type' => 'application/json',
                        'Authorization' => "Bearer $apiKey",
                    ])->timeout(500)->post('https://api.openai.com/v1/engines/text-davinci-003/completions', [
                        'prompt' => 'Please classify following text in {} into only one of the following topics \\n' . $topics . ' : {' . $chunk . '}',
                        'max_tokens' => (int) (strlen($chunk) / 4 +25),
                        'temperature' => 0,
                    ]);
                } catch (\Throwable $th) {
                    $id = $news->id;
                    dump('error, skipped', $id);
                    $news->delete();
                    continue;
                }
               

                // Обработка ошибок
                if ($response->failed()) {
                    dump($response->json('error'));
                    $request_failed = true;
                    $request_count++;
                    sleep(2 * $request_count);
                    continue;
                }

                $response_data = $response->json();
                $topic = Str::of($response_data['choices'][0]['text'])->trim()->ucfirst();

                $request_failed = false;

            } while ($request_failed && $request_count < 3);
            

            if(!in_array($topic, $needTopic)) {
                dump('skipped news with topic:', $topic);
                $news->is_processed = true;
                $news->topic = $topic;
                $news->save();
                continue;
            } else {
                dump('add news with topic:', $topic);
            }

            $news->topic = $topic;
            $news->save();

            dump('full content length ' . strlen($message_text));

            // Разбиение сообщения на части по 2048 токенов
            $message_parts = $this->splitTextIntoChunks($this->removeExtraSpacesAndLineBreaks($message_text), 3900);

            // // Отправка запросов к ChatGPT API по частям сообщения
            // sleep(20);

            // Отправка запросов к ChatGPT API по частям сообщения
            $shortContent = '';

            foreach ($message_parts as $key => $part) {
                $partNumber = $key+1;
                // dump(["step $partNumber", strlen($part), $part]);
                $request_count = 0;
                $request_failed = false;
    
                do {
                    // Отправка запроса к ChatGPT API
                    $response = Http::retry(3, 20000)->withHeaders([
                        'Content-Type' => 'application/json',
                        'Authorization' => "Bearer $apiKey",
                    ])->timeout(500)->post('https://api.openai.com/v1/engines/text-davinci-003/completions', [
                        'prompt' => 'Please extract the most important information (max 200 chars) from the following news article in English: ' . $part,
                        'max_tokens' => (int) (strlen($part) / 4 + 70),
                        'temperature' => 0,
                    ]);
    
                    // Обработка ошибок
                    if ($response->failed()) {
                        dump($response->json('error'));
                        $request_failed = true;
                        $request_count++;
                        sleep(2 * $request_count);
                        continue;
                    }
    
                    $response_data = $response->json();
                    $shortContent .= $response_data['choices'][0]['text'];
    
                    $request_failed = false;
    
                } while ($request_failed && $request_count < 3);
            }

            // Разбиение сообщения на части по 2048 токенов
            $message_parts = $this->splitTextIntoChunks($this->removeExtraSpacesAndLineBreaks($shortContent), 3900);


            // Отправка запросов к ChatGPT API по частям сообщения
            $extra = '';
            foreach ($message_parts as $key => $part) {
                $partNumber = $key+1;
                $request_count = 0;
                $request_failed = false;

                do {
                    // Отправка запроса к ChatGPT API
                    $response = Http::retry(3, 20000)->withHeaders([
                        'Content-Type' => 'application/json',
                        'Authorization' => "Bearer $apiKey",
                    ])->timeout(500)->post('https://api.openai.com/v1/engines/text-davinci-003/completions', [
                        'prompt' => 'Make a summary in two paragraphs (max 450 chars) for following text in English: ' . $part,
                        'max_tokens' => (int) (strlen($part) / 4 +200),
                        'temperature' => 0,
                    ]);

                    // Обработка ошибок
                    if ($response->failed()) {
                        dump($response->json('error'));
                        $request_failed = true;
                        $request_count++;
                        sleep(2 * $request_count);
                        continue;
                    }

                    $response_data = $response->json();
                    $extra .= $response_data['choices'][0]['text'];

                    $request_failed = false;

                } while ($request_failed && $request_count < 3);
            }

            $message_parts = $this->splitTextIntoChunks($this->removeExtraSpacesAndLineBreaks($extra), 3900);

            $quotes = '';
            foreach ($message_parts as $part) {
                if($part == '') {
                    continue;
                } 

                $request_count = 0;
                $request_failed = false;

                do {
                    // Отправка запроса к ChatGPT API
                    $response = Http::retry(3, 20000)->withHeaders([
                        'Content-Type' => 'application/json',
                        'Authorization' => "Bearer $apiKey",
                    ])->timeout(500)->post('https://api.openai.com/v1/engines/text-davinci-003/completions', [
                        'prompt' => 'Please extract and format 3 key points in a numbered list, ordered by importance and informational value: {' . $part . '}',
                        'max_tokens' => (int) (strlen($part) / 4 + 50),
                        'temperature' => 0,
                    ]);

                    // Обработка ошибок
                    if ($response->failed()) {
                        dump($response->json('error'));
                        $request_failed = true;
                        $request_count++;
                        sleep(2 * $request_count);
                        continue;
                    }

                    $response_data = $response->json();
                    $quotes .= $response_data['choices'][0]['text'];

                    $request_failed = false;

                } while ($request_failed && $request_count < 3);
            }

            // sleep(20);

            $short = '';

            foreach ($message_parts as $key => $part) {
                $partNumber = $key+1;
                // dump(["step $partNumber", strlen($part), $part]);
                $request_count = 0;
                $request_failed = false;
    
                do {
                    // Отправка запроса к ChatGPT API
                    $response = Http::retry(3, 20000)->withHeaders([
                        'Content-Type' => 'application/json',
                        'Authorization' => "Bearer $apiKey",
                    ])->timeout(500)->post('https://api.openai.com/v1/engines/text-davinci-003/completions', [
                        'prompt' => 'the main conclusion of the same article in one sentence (max 120 chars) in English: ' . $part,
                        'max_tokens' => (int) (strlen($part) / 4 +50),
                        'temperature' => 0,
                    ]);
    
                    // Обработка ошибок
                    if ($response->failed()) {
                        dump($response->json('error'));
                        $request_failed = true;
                        $request_count++;
                        sleep(2 * $request_count);
                        continue;
                    }
    
                    $response_data = $response->json();
                    $short .= $response_data['choices'][0]['text'];
    
                    $request_failed = false;
    
                } while ($request_failed && $request_count < 3);
            }

            $redneck = '';
            foreach ($message_parts as $part) {
                if (!strlen($part)) {
                    continue;
                } 

                $request_count = 0;
                $request_failed = false;
    
                do {
                    // Отправка запроса к ChatGPT API
                    $response = Http::retry(3, 20000)->withHeaders([
                        'Content-Type' => 'application/json',
                        'Authorization' => "Bearer $apiKey",
                    ])->timeout(500)->post('https://api.openai.com/v1/engines/text-davinci-003/completions', [
                        'prompt' => 'Format the following text in the conversational like for a five year child  with emoji and please make this text simplify (max 150 chars), while retaining all the useful information, and translate it into English:' . $part,
                        'max_tokens' => (int) (strlen($part) / 4 * 2),
                        'temperature' => 0,
                    ]);
    
                    // Обработка ошибок
                    if ($response->failed()) {
                        dump($response->json('error'));
                        $request_failed = true;
                        $request_count++;
                        sleep(2 * $request_count);
                        continue;
                    }
    
                    $response_data = $response->json();
                    $redneck .= $response_data['choices'][0]['text'];
    
                    $request_failed = false;
    
                } while ($request_failed && $request_count < 3);
            }
            
            // sleep(20);

            $tags = '';
            foreach ($message_parts as $part) {
                $request_count = 0;
                $request_failed = false;
    
                do {
                    // Отправка запроса к ChatGPT API
                    $response = Http::retry(3, 20000)->withHeaders([
                        'Content-Type' => 'application/json',
                        'Authorization' => "Bearer $apiKey",
                    ])->timeout(500)->post('https://api.openai.com/v1/engines/text-davinci-003/completions', [
                        'prompt' => 'Create a numbered list of 10 as short (max 1 or 2 words and max 14 chars) unique tags related to the theme and main content, ordered by their popularity in English without the "#" symbol in the following text: {' . $part . '}',
                        'max_tokens' => (int) (strlen($part) / 4 + 50),
                        'temperature' => 0,
                        'stop' => ['\n'],
                    ]);
    
                    // Обработка ошибок
                    if ($response->failed()) {
                        dump($response->json('error'));
                        $request_failed = true;
                        $request_count++;
                        sleep(2 * $request_count);
                        continue;
                    }
    
                    $response_data = $response->json();
                    $tags .= $response_data['choices'][0]['text'];
    
                    $request_failed = false;
    
                } while ($request_failed && $request_count < 3);
            }

            dump([
                'quotes' => $this->createNumberedList($this->formatTextIntoArray($quotes)),
                'short' => $short,
                'extra' => $extra,
                'redneck' => $redneck,
                'tags' => $this->createNumberedList($this->formatTextIntoArray($tags)),
            ]);

            $news->content_quote_en = $this->createNumberedList($this->formatTextIntoArray($quotes));
            $news->content_ext_en = $extra;
            $news->content_short_en = $short;
            $news->topic = $topic;
            $news->content_redneck_en = $redneck;
            $news->is_processed = true;

            $news->save();

            $tags = $this->formatTextIntoArray($tags);


            foreach ($tags as $tag) {
                if(strlen($tag) < 15 && !is_numeric($tag)) {
                    $tag = Str::of($tag)->trim()->ucfirst();
                    $news->attachTagByNameAndTopic($tag);
                }
            }

            $id = $news->id;

            dump("successed formating $id");
        }
    }

    protected function splitTextIntoChunks($text, $maxChunkLength) {
        $lines = preg_split('/(\.|\n)/', $text);
        $chunks = [];
        $currentChunk = '';
    
        $count = 0;

        foreach ($lines as $key => $line) {
            if($key == 0) {
                continue;
            }
            
            if (strlen($currentChunk) + strlen($line) + 1 <= $maxChunkLength) {
                $currentChunk .= $line . PHP_EOL;
            } else {
                $count = $count + 1;
                $chunks[] = $currentChunk;
                $currentChunk = $line . PHP_EOL;
                if($count > 2) {
                    return $chunks;
                }
            }
        }
    
        if (!empty($currentChunk)) {
            $chunks[] = $currentChunk;
        }
    
        return $chunks;
    }

    protected function formatTextIntoArray($text) {
        $formattedLine = preg_replace('/\s?\d+\./', '##', $text);;
        $lines = explode('##', $formattedLine);
        $formattedLines = [];
    
        foreach ($lines as $key => $line) {
            if($key == 0) {
                continue;
            }

            $formattedLine = preg_replace('/^\d+\./', '##', $line);
            $formattedLine = trim($formattedLine); // добавлено: удаление пробелов в начале строки
            if (!empty($formattedLine)) {
                $formattedLines[] = $formattedLine;
            }
        }
    
        return $formattedLines;
    }
    
    protected function createNumberedList($lines)
    {
        $numberedList = '';

        foreach ($lines as $index => $line) {
            $numberedList .= ($index + 1) . '. ' . $line . PHP_EOL;
        }

        return $numberedList;
    }

    protected function removeExtraSpacesAndLineBreaks($text) {
        $text = preg_replace('/\s+/', ' ', $text); // Заменяем все пробельные символы на один пробел
        $text = preg_replace('/[\r\n]+/', PHP_EOL, $text); // Заменяем все подряд идущие символы переноса строки на один символ переноса
        $text = trim($text); // Удаляем пробелы и символы переноса строки с начала и конца текста
        return $text;
    }
}
