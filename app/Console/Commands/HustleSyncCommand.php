<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Arr;
use App\Models\News;
use App\Jobs\ProcessNewsJob;
use Symfony\Component\DomCrawler\Crawler;
use Illuminate\Support\Carbon;
use GuzzleHttp\Client;

class HustleSyncCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:hustle';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start of sync command';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $topicsToTags = array(
            'Business' => array('advertising', 'agriculture', 'airlines', 'banking', 'bankruptcy', 'books', 'Branded Content', 'branding', 'cannabis', 'communication', 'construction', 'consumer products', 'corporate profits', 'cpg', 'cruise', 'D2C', 'economy', 'energy', 'entrepreneurs', 'Finance', 'Financial Tech', 'franchise', 'gambling', 'grocery', 'housing and real estate', 'inflation', 'insurance', 'International', 'labor', 'landlords', 'lawsuits', 'layoffs', 'legal issues', 'lending', 'logistics', 'Luxury Goods', 'manufacturing', 'mastercard', 'publishing', 'Publishing and Marketing', 'Regulation', 'rent', 'restaurants', 'retirement', 'right to repair', 'saas', 'sales', 'Science and research', 'silicon valley', 'Silicon Valley Bank', 'small businesses', 'supply chain', 'surveillance', 'sustainability', 'Tesla', 'Transportation', 'Travel and Leisure', 'trucks', 'Tulsa', 'VC', 'vehicles', 'Venture Capital', 'VISA', 'wealth', 'weather', 'WiFi', 'work', 'Workplace Issues', 'yachts'),
            'Entertainment' => array('animals', 'beauty', 'books', 'cartoons', 'celebrity', 'charity', 'Climate', 'clothing', 'Clubhouse', 'commodities', 'creators', 'data', 'dating', 'delivery', 'dentistry', 'depop', 'design', 'digits', 'Disney', 'diversity', 'Dungeons & Dragons', 'Ecommerce', 'Ecommerce and Retail', 'elderly', 'Emerging Tech', 'entertainment', 'environment', 'exercise', 'fashion', 'fitness', 'Food', 'Gen Z', 'Geography', 'health and wellness', 'hidead', 'holiday', 'inclusivity', 'influencers', 'infrastructure', 'media', 'media & publishing', 'mental health', 'metaverse', 'milk', 'Money and Markets', 'movies', 'MSCHF', 'museums', 'music', 'netflix', 'nohome', 'nostalgia', 'pets', 'Pharmaceuticals', 'piracy', 'podcast', 'predictions', 'Privacy Issues', 'Religion', 'rideshare', 'shopping', 'Social networking', 'space', 'Sports and esports', 'spotify', 'stocks', 'streaming media', 'studies', 'subscriptions', 'technology', 'television', 'tiktok', 'tipping', 'toys', 'tradition', 'trends', 'twitter', 'voice notes', 'VR', 'waymo', 'zoom'),
            'Cryptocurrency' => array('cryptocurrency'),
            'Startups' => array('startup'),
            'Government' => array('government'),
            'Health' => array('healthcare', 'lgbtq+', 'medical', 'medicine', 'wellness')
        );

        
            $page = 1; // начинаем с первой страницы

            $parsedItems = [];

            $client = new Client();

            do {
                $url = "https://thehustle.co/series/brief/page/{$page}";
                $response = $client->get($url);
                $html = $response->getBody()->getContents();
                $crawler = new Crawler($html);

                $newsTags = $crawler->filter('.c-post-box__tag a')->each(function (Crawler $node) {
                    return $node->text();
                });
            
                // Извлекаем ссылки на новости
                $newsLinks = $crawler->filter('.c-post-box__title a')->each(function (Crawler $node) {
                    return $node->attr('href');
                });
            
                // Обрабатываем каждую новость
                foreach ($newsLinks as $key => $newsLink) {
                    try {
                        $response = $client->get($newsLink);
                        $html = $response->getBody()->getContents();
                        $newsCrawler = new Crawler($html);
                
                        $title = @$newsCrawler->filter('.c-post__title')->text();
   
                        $foundTopic = $this->findTopicByTag(Arr::get($newsTags, $key, ''), $topicsToTags);

                        if ($foundTopic) {
                           $category = $foundTopic;
                        } else {
                            $category = 'Other';
                        }

                        $imageNode = @$newsCrawler->filter('.single-wrap .single-thumb img');
                        $image = @$imageNode->count() ? $imageNode->attr('data-src') : null;
                        
                        $date = @$newsCrawler->filter('.c-post__time')->text();
                        
                
                        $contentNode = @$newsCrawler->filter('.s-content');

                        $contentNode->filter('div')->each(function (Crawler $crawler) {
                            foreach ($crawler as $node) {
                                $node->parentNode->removeChild($node);
                            }
                        });

                        $content = @$contentNode->text();

                        if ($title && $content && $newsLink && $category && $date && $image) {
                            dump([
                                'title' => $title,
                                'content' => $content,
                                'link' => $newsLink,
                                'category' => $category,
                                'date' => $date,
                                'image' => $image,
                            ]);

                            $parsedItems[] = [
                                'title' => $title,
                                'content' => $content,
                                'link' => $newsLink,
                                'category' => $category,
                                'date' => $date,
                                'image' => $image,
                            ];
                        }
                    } catch (\Throwable $th) {
                        throw $th;
                    }
                    
                }
            
                // Увеличиваем номер страницы
                $page++;

                $this->saveNewsItems($parsedItems);
            
                $parsedItems = [];
                
            } while ($page <= 20); 
    }

    function findTopicByTag($tag, $topicsToTags) {
        foreach ($topicsToTags as $topic => $tags) {
            if (in_array($tag, $tags)) {
                return $topic;
            }
        }
        return null;
    }

    function parseArticleContent($url)
    {
        $response = Http::get($url);

        if ($response->successful()) {
            $html = $response->body();
            $crawler = new Crawler($html);

            $content = $crawler->filter('.post-content')->text();

            return $content;
        }

        return '';
    }


    function saveNewsItems($parsedItems)
    {
        foreach ($parsedItems as $item) {
            News::firstOrCreate([
                'source_url' => Arr::get($item, 'link', ''),
            ], [
                'name' => Arr::get($item, 'title', ''),
                'content_en' => Arr::get($item, 'content', ''),
                'source_url' => Arr::get($item, 'link', ''),
                'source' => 'TheHustle',
                'topic' => Arr::get($item, 'category', ''),
                'preview' => Arr::get($item, 'image', ''),
                'created_at' => Carbon::parse(Arr::get($item, 'date', Carbon::now())),
                'updated_at' => Carbon::parse(Arr::get($item, 'date', Carbon::now())),
                'lang' => 'en',
            ]);
        }
    }

    protected function urljoin($base, $relative)
    {
        if (parse_url($relative, PHP_URL_SCHEME) !== null) {
            return $relative;
        }

        $parts = parse_url($base);

        if ($relative[0] == '/') {
            return $parts['scheme'] . '://' . $parts['host'] . $relative;
        }

        $path = isset($parts['path']) ? preg_replace('#/[^/]*$#', '', $parts['path']) : '';

        while (substr($relative, 0, 3) === '../') {
            $relative = substr($relative, 3);
            $path = preg_replace('#/[^/]*$#', '', $path);
        }

        return $parts['scheme'] . '://' . $parts['host'] . $path . '/' . $relative;
    }
}
