<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Arr;
use App\Models\News;
use App\Jobs\ProcessNewsJob;
use Symfony\Component\DomCrawler\Crawler;
use Illuminate\Support\Carbon;

class MediumSyncCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:medium';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start of sync command';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $accessToken = config('services.medium.access_token');

        $categories = [
            '7b2438b07d33' => 'Business',
        ];

        foreach($categories as $key => $category) {
            $url = "https://medium.com/_/api/home-feed?limit=200&topicId={$key}";
 

            try {
                $response = Http::retry(10, 5000)->get($url); 
            } catch (RequestException $e) {
                // you need to still handle errors here
            }
        
            $data = json_decode(str_replace('])}while(1);</x>', '', $response->body()), true);
        
            $parsedItems = [];
        
            if (isset($data['payload']['references']['Post'])) {
                $posts = $data['payload']['references']['Post'];
        
                foreach ($posts as $post) {
                    $title = $post['title'] ?? '';
                    $content = $post['virtuals']['subtitle'] ?? '';
                    $link = 'https://medium.com/p/' . $post['id'];
                    $date = $post['createdAt'] ?? '';
                    $image = $post['virtuals']['previewImage']['imageId'] ? 'https://cdn-images-1.medium.com/fit/t/1600/480/' . $post['virtuals']['previewImage']['imageId'] : '';


                    $exists = News::where([
                        'source_url' => $link,
                    ])->count();

                    if($exists) {
                        dump($link . ' exists');
                        continue;
                    }

                    try {
                        $response = Http::retry(10, 3000)->get($link); 
                    } catch (RequestException $e) {
                        // you need to still handle errors here
                    }

                    $content = '';

                    if ($response->successful()) {
                        $html = $response->body();
                        $crawler = new Crawler($html);

                        $div = $crawler->filter('article section')->getNode(0);
                        $domXpath = new \DOMXPath($div->ownerDocument);

                        $textNodes = $domXpath->query('descendant::text()', $div);

                        foreach ($textNodes as $textNode) {
                            $content .= $textNode->textContent;
                        }
                    }

                    if (!$content || strlen($content) < 550) {
                        continue;
                    } 

                    dump([
                        'title' => $title,
                        'content' => $content,
                        'link' => $link,
                        'category' => $category,
                        'date' => $date,
                        'image' => $image,
                    ]);

                    $parsedItems[] = [
                        'title' => $title,
                        'content' => $content,
                        'link' => $link,
                        'category' => $category,
                        'date' => $date,
                        'image' => $image,
                    ];
                    
                    $this->saveNewsItems($parsedItems);
                    $parsedItems = [];
                    sleep(50);
                }
            }

            sleep(100);
        }
       
    
        return $parsedItems;
    }


    function saveNewsItems($parsedItems)
    {
        foreach ($parsedItems as $item) {
            News::firstOrCreate([
                'source_url' => Arr::get($item, 'link', ''),
            ],
            [
                'name' => Arr::get($item, 'title', ''),
                'content_en' => Arr::get($item, 'content', ''),
                'source_url' => Arr::get($item, 'link', ''),
                'source' => 'Medium',
                'topic' => Arr::get($item, 'category', ''),
                'preview' => Arr::get($item, 'image', ''),
                'created_at' => Carbon::createFromTimestamp(Arr::get($item, 'date') / 1000),
                'updated_at' => Carbon::createFromTimestamp(Arr::get($item, 'date') / 1000),
                'lang' => 'en',
            ]);
        }
    }

    protected function urljoin($base, $relative)
    {
        if (parse_url($relative, PHP_URL_SCHEME) !== null) {
            return $relative;
        }

        $parts = parse_url($base);

        if ($relative[0] == '/') {
            return $parts['scheme'] . '://' . $parts['host'] . $relative;
        }

        $path = isset($parts['path']) ? preg_replace('#/[^/]*$#', '', $parts['path']) : '';

        while (substr($relative, 0, 3) === '../') {
            $relative = substr($relative, 3);
            $path = preg_replace('#/[^/]*$#', '', $path);
        }

        return $parts['scheme'] . '://' . $parts['host'] . $path . '/' . $relative;
    }
}
