<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Arr;
use App\Models\News;
use App\Jobs\ProcessNewsJob;
use Carbon\Carbon;
use Symfony\Component\DomCrawler\Crawler;


class SyncGNewsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:gnews';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start of sync command';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $sources = [
            'associated-press',
            'bbc-news',
            'bild',
            'bleacher-report',
            'bloomberg',
            'business-insider',
            'buzzfeed',
            'cbc-news',
            'cnn',
            'engadget',
            'espn',
            'fortune',
            'hacker-news',
            'independent',
            'lequipe',
            'mashable',
            'national-geographic',
            'national-review',
            'newsweek',
            'new-york-magazine',
            'politico',
            'reddit-r-all',
            'reuters',
            'techcrunch',
            'techradar',
            'the-huffington-post',
            'the-verge',
        ];

        dump('start sync');

        foreach ($sources as $source) { 
            dump("Current source is $source");

            $page = 1;

            do {
                $response = Http::get('https://newsapi.org/v2/everything', [
                    'apiKey' => '35c6142631174efab825f2d0da1013d7',
                    'q' => '*',
                    'pageSize' => '100',
                    'language' => 'en',
                    'page' => $page,
                    'sources' => $source,
                    'from' => '2023-05-01',
                ]);
                    
                if ($response->successful()) { 
    
                    $news = $response->json('articles');
                        foreach ($news as $item) {
                            $content = Arr::get($item, 'description', '') . PHP_EOL . Arr::get($item, 'content', '');
    
                            dump($content, Carbon::parse(Arr::get($item, 'publishedAt')));
                
                            if (!$content || strlen($content) < 150) {
                                continue;
                            }
                
                            $newsItem = News::updateOrCreate([
                                'source_url' => Arr::get($item, 'url', ''),
                            ],
                            [
                                'name' => Arr::get($item, 'title', ''),
                                'content_en' => PHP_EOL . Arr::get($item, 'description', '') . PHP_EOL . Arr::get($item, 'content', ''),
                                'source_url' => Arr::get($item, 'url', ''),
                                'source' => Arr::get($item, 'source.name', ''),
                                'topic' => 'Other',
                                'preview' => Arr::get($item, 'urlToImage', ''),
                                'created_at' => Carbon::parse(Arr::get($item, 'publishedAt')),
                                'updated_at' => Carbon::parse(Arr::get($item, 'publishedAt')),
                                'lang' => 'en',
                            ]);
                    }
        
                    $page++;

                    dump("sync comleted $page Page");
                    dump("Current source is $source");
                }

            } while ($page <= 10);


            $response = Http::get('https://newsapi.org/v2/top-headlines', [
                'apiKey' => '35c6142631174efab825f2d0da1013d7',
                'pageSize' => '100',
                'language' => 'en',
                'page' => 1,
                'sources' => $source,
            ]);
                
            sleep(2);

            if ($response->successful()) { 

                $news = $response->json('articles');
                    foreach ($news as $item) {
                        $content = Arr::get($item, 'description', '') . PHP_EOL . Arr::get($item, 'content', '');

                        dump($content, Carbon::parse(Arr::get($item, 'publishedAt')));
            
                        if (!$content || strlen($content) < 100) {
                            continue;
                        }
            
                        $newsItem = News::firstOrCreate([
                            'source_url' => Arr::get($item, 'url', ''),
                        ],
                        [
                            'name' => Arr::get($item, 'title', ''),
                            'content_en' => PHP_EOL . Arr::get($item, 'description', '') . PHP_EOL . Arr::get($item, 'content', ''),
                            'source_url' => Arr::get($item, 'url', ''),
                            'source' => Arr::get($item, 'source.name', ''),
                            'topic' => 'Other',
                            'preview' => Arr::get($item, 'urlToImage', ''),
                            'created_at' => Carbon::parse(Arr::get($item, 'publishedAt')),
                            'updated_at' => Carbon::parse(Arr::get($item, 'publishedAt')),
                            'lang' => 'en',
                        ]);
                }
    
                dump('sync comleted');
            }
        }
    }

    function extractContentFromUrl($url) {
        // Загрузите HTML-контент страницы
        $html = @file_get_contents($url);
        
        $crawler = new Crawler($html);

        $mostTextNode = null;
        $maxTextLength = 0;
    
        // Проход по всем нодам в DOM-дереве
        $crawler->filter('body *')->each(function (Crawler $node, $i) use (&$mostTextNode, &$maxTextLength) {
            // Получаем дочерние текстовые ноды
            $childTextNodes = $node->filterXPath('//*[not(self::script or self::style)]/text()');
    
            // Считаем суммарную длину текста в дочерних нодах
            $textLength = 0;
            foreach ($childTextNodes as $childTextNode) {
                $trimmedText = trim($childTextNode->nodeValue);
                $textLength += strlen($trimmedText);
            }
    
            // Если длина текста больше, чем у предыдущего максимума, обновляем значения
            if ($textLength > $maxTextLength) {
                $maxTextLength = $textLength;
                $mostTextNode = $node;
            }
        });

        return $mostTextNode->text();
    }
}
