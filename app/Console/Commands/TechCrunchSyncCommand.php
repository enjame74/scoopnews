<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Arr;
use App\Models\News;
use App\Jobs\ProcessNewsJob;
use Symfony\Component\DomCrawler\Crawler;
use Illuminate\Support\Carbon;

class TechCrunchSyncCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:techcrunch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start of sync command';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        // News::where('is_processed', false)->get()->each->delete();
        dump('start sync');

        $page = 1;
        $hasNextPage = true;

        while ($hasNextPage) {
            $url = "https://techcrunch.com/2023/05/page/{$page}/";
            $parsedItems = $this->parseTechCrunch($url);

            dump('current page ' .  $page);

            if (count($parsedItems) > 0) {
                $this->saveNewsItems($parsedItems);
                $page++;
            } else {
                $hasNextPage = false;
            }
        }
    }

    protected function parseTechCrunch($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $html = curl_exec($ch);
        curl_close($ch);

        $crawler = new Crawler($html);
        $newsItems = $crawler->filter('.post-block');

        $parsedItems = [];

        foreach ($newsItems as $newsItem) {
            $itemCrawler = new Crawler($newsItem);

            $title = $itemCrawler->filter('.post-block__title__link')->text();
            $category = 'technology';
            $date = $itemCrawler->filter('.river-byline__time')->attr('datetime');
            $image = $itemCrawler->filter('.post-block__media img')->attr('src');

        
            $link = $itemCrawler->filter('.post-block__title__link')->attr('href');

            $exists = News::where([
                'source_url' => $link,
            ])->count();

            if($exists) {
                dump($link . ' exists');
                continue;
            }
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $link);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $innerHtml = curl_exec($ch);
            curl_close($ch);

            $innerCrawler = new Crawler($innerHtml);

            $title = $innerCrawler->filter('.article__title')->text();
            $content = $innerCrawler->filter('.article-content')->text();
            $category = 'Technology';

            $parsedItems[] = [
                'title' => $title,
                'content' => $content,
                'link' => $link,
                'category' => $category,
                'date' => $date,
                'image' => $image,
            ];

            sleep(20);
        }

        return $parsedItems;
    }

    function saveNewsItems($parsedItems)
    {
        foreach ($parsedItems as $item) {
            News::firstOrCreate([
                'source_url' => Arr::get($item, 'link', ''),
            ],[
                'name' => Arr::get($item, 'title', ''),
                'content_en' => Arr::get($item, 'content', ''),
                'source_url' => Arr::get($item, 'link', ''),
                'source' => 'Tech Crunch',
                'topic' => Arr::get($item, 'category', ''),
                'preview' => Arr::get($item, 'image', ''),
                'created_at' => Carbon::parse(Arr::get($item, 'date', Carbon::now())),
                'updated_at' => Carbon::parse(Arr::get($item, 'date', Carbon::now())),
                'lang' => 'en',
            ]);
        }
    }
}
