<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreNewsRequest;
use App\Http\Requests\UpdateNewsRequest;
use Illuminate\Http\Request;
use App\Models\News;
use App\Models\Tags;
use Illuminate\Database\Eloquent\Builder;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $date = session()->get('date', 'All time');
        $searchText = session()->get('search', '');

        $filter = session()->get('filter', null);

        $from = null;
        $search = null;

        if($date == 'Today') {
            $from = \Carbon\Carbon::now()->startOfDay();
        }

        if($date == 'Week') {
            $from = \Carbon\Carbon::now()->subWeek()->startOfDay();
        }

        if($date == 'Month') {
            $from = \Carbon\Carbon::now()->subMonth()->startOfDay();
        }

        if ($searchText ) {
            $search = $searchText;
        }

        $categories = [];
        $selectedTags = [];

        if ($filter) {
            foreach ($filter as $category => $tags) {
                if($tags == 'all') {
                    $categories[] = $category;
                } else {
                    $selectedTags = array_merge($selectedTags, $tags);
                }
            }
        }

        $newsWithTopTags = News::whereHas('tags', function($query) use ($selectedTags, $categories) {
            if(count($categories)) {
                $query->whereIn('topic', $categories);
            }
            if(count($selectedTags)) {
                $query->orWhereIn('name', $selectedTags);
            }
        })->with(['tags' => function ($query) use ($from, $search) {
            $query->select('tags.*', 'news_tags.news_id as pivot_news_id', 'news_tags.tags_id as pivot_tags_id')
                ->join('news_tags as nt', 'tags.id', '=', 'nt.tags_id')
                ->whereIn('tags.id', function ($query) {
                    $query->select('tags.id')
                        ->from('tags')
                        ->join('news_tags', 'news_tags.tags_id', '=', 'tags.id')
                        ->whereColumn('news_tags.news_id', 'nt.news_id')
                        ->groupBy('tags.id')
                        ->orderByRaw('SUM(tags.count) DESC');
                });
        }])
            ->when($from, function (Builder $query) use ($from){
                $query->where('created_at', '>=' , $from);
            })
            ->when($search, function (Builder $query) use ($search){
                $query->where(function($q) use ($search){
                    $q->where('name', 'ilike', '%'.$search.'%')
                    ->orWhere('content_quote_en', 'ilike', '%'.$search.'%')
                    ->orWhere('content_ext_en', 'ilike', '%'.$search.'%')
                    ->orWhere('content_short_en', 'ilike', '%'.$search.'%')
                    ->orWhere('content_redneck_en', 'ilike', '%'.$search.'%');
                });
            })
            ->where('is_processed', true)
            ->orderBy('created_at', 'DESC')
            ->paginate(10);

        $tagsGroupedByTopics = Tags::select('tags.topic', 'tags.name', 'tags.count')
            ->selectRaw('MAX(tags.count) as max_count')
            ->whereRaw('(SELECT COUNT(news_tags.news_id) FROM news_tags WHERE news_tags.tags_id = tags.id) > 1')
            ->groupBy('tags.topic', 'tags.name', 'tags.count')
            ->orderBy('max_count', 'desc')
            ->get()
            ->groupBy('topic')
            ->sortBy('topic')
            ->map(function ($group) {
                return $group->unique('name')->take(20);
            })->sortBy(function ($item, $key) {
                return $key;
            });

        return view('news')
            ->with([
                'news' => $newsWithTopTags,
                'tags' => $tagsGroupedByTopics,
                'loaderUri' => "/news/loadData/",
                'url' =>  "/news",
            ]);
    }



    public function loadData(Request $request)
    {
        $date = session()->get('date', 'All time');
        $searchText = session()->get('search', '');

        $from = null;
        $search = null;

        $filter = session()->get('filter', null);


        $categories = [];
        $selectedTags = [];

        if ($filter) {
            foreach ($filter as $category => $tags) {
                if($tags == 'all') {
                    $categories[] = $category;
                } else {
                    $selectedTags = array_merge($selectedTags, $tags);
                }
            }
        }

        if($date == 'Today') {
            $from = \Carbon\Carbon::now()->startOfDay();
        }

        if($date == 'Week') {
            $from = \Carbon\Carbon::now()->subWeek()->startOfDay();
        }

        if($date == 'Month') {
            $from = \Carbon\Carbon::now()->subMonth()->startOfDay();
        }

        if ($searchText ) {
            $search = $searchText;
        }

        // Получаем данные для загрузки с указанной страницы
        $newsWithTopTags = News::whereHas('tags', function($query) use ($selectedTags, $categories) {
            if(count($categories)) {
                $query->whereIn('topic', $categories);
            }
            if(count($selectedTags)) {
                $query->orWhereIn('name', $selectedTags);
            }
        })->with(['tags' => function ($query) {
            $query->select('tags.*', 'news_tags.news_id as pivot_news_id', 'news_tags.tags_id as pivot_tags_id')
                ->join('news_tags as nt', 'tags.id', '=', 'nt.tags_id')
                ->whereIn('tags.id', function ($query) {
                    $query->select('tags.id')
                        ->from('tags')
                        ->join('news_tags', 'news_tags.tags_id', '=', 'tags.id')
                        ->whereColumn('news_tags.news_id', 'nt.news_id')
                        ->groupBy('tags.id')
                        ->orderByRaw('SUM(tags.count) DESC');
                });
        }])
            ->when($from, function (Builder $query) use ($from){
                $query->where('created_at', '>=' , $from);
            })
            ->when($search, function (Builder $query) use ($search){
                $query->where(function($q) use ($search){
                    $q->where('name', 'ilike', '%'.$search.'%')
                    ->orWhere('content_quote_en', 'ilike', '%'.$search.'%')
                    ->orWhere('content_ext_en', 'ilike', '%'.$search.'%')
                    ->orWhere('content_short_en', 'ilike', '%'.$search.'%')
                    ->orWhere('content_redneck_en', 'ilike', '%'.$search.'%');
                });
            })
            ->where('is_processed', true)
            ->orderBy('created_at', 'DESC')
            ->paginate(10);


        return view('loader')
            ->with([
                'news' => $newsWithTopTags,
            ]);
    }

    public function debug()
    {
        $newsWithTopTags = News::orderBy('created_at', 'DESC')->take(500)->get();

        $categories = [];

        return view('debug')
            ->with([
                'news' => $newsWithTopTags,
                'categories' => $categories,
            ]);
    }
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreNewsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $newsItem = News::with(['tags' => function ($query) {
            $query->select('tags.*', 'news_tags.news_id as pivot_news_id', 'news_tags.tags_id as pivot_tags_id')
                ->join('news_tags as nt', 'tags.id', '=', 'nt.tags_id')
                ->whereIn('tags.id', function ($query) {
                    $query->select('tags.id')
                        ->from('tags')
                        ->join('news_tags', 'news_tags.tags_id', '=', 'tags.id')
                        ->whereColumn('news_tags.news_id', 'nt.news_id')
                        ->groupBy('tags.id')
                        ->orderByRaw('SUM(tags.count) DESC');
                });
        }])->where('is_processed', true)->findOrFail($id);

        return view('news-item')
            ->with([
                'newsItem' => $newsItem
            ]);
    }

    public function mode(Request $request)
    {
        $request->session()->put('mode', $request->get('mode'));

        return redirect()->to($request->get('url'));
    }

    public function date(Request $request)
    {
        $request->session()->put('date', $request->get('mode'));

        return redirect()->to($request->get('url'));
    }

    public function filter(Request $request)
    {        
        $request->session()->put('filter', $request->get('filter'));

        return redirect()->to('/news');
    }

    public function search(Request $request)
    {
        $request->session()->put('search', $request->get('search'));

        return redirect()->to($request->get('url'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(News $news)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateNewsRequest $request, News $news)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(News $news)
    {
        //
    }
}
