<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'content',
        'content_quote',
        'content_ext',
        'content_redneck',
        'content_en',
        'content_quote_en',
        'content_ext_en',
        'content_redneck_en',
        'topic',
        'author',
        'preview',
        'source_url',
        'source',
        'is_processed',
        'lang',
        'created_at',
        'updated_at',
    ];

    public function tags()
    {
        return $this->belongsToMany(Tags::class, 'news_tags', 'news_id', 'tags_id');
    }

    public function attachTagByNameAndTopic($tagName)
    {
        $tag = Tags::firstOrCreate([
            'name' => $tagName,
            'topic' => $this->topic,
        ]);
    
        // Инкрементируем счетчик, если тег еще не связан с этой новостью
        if (!$this->tags->contains($tag)) {
            $tag->increment('count');
        }
    
        // Используем метод syncWithoutDetaching для добавления связи, если она не существует
        $this->tags()->syncWithoutDetaching([$tag->id]);
    }
}
